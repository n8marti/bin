#!/usr/bin/env python3

import urllib.request
import json
from sys import stdout
from time import sleep

host = 'http://192.168.3.1'
get_addr = host + '/goform/goform_get_cmd_process'
referer = host + '/index.html'

rate = 'realtime_rx_thrpt'
rate_data = {'cmd': rate}
data = urllib.parse.urlencode(rate_data).encode('ascii')

headers = {
    'Host': host,
    'Referer': referer,
}

def convert(B_amt):
    B_amt = float(B_amt)
    KB_amt = round(B_amt / 2**10, 3)
    MB_amt = round(KB_amt / 2**10, 3)

    if MB_amt > 1:
        output = str(MB_amt) + ' MB/s  '
    elif KB_amt > 1:
        output = str(KB_amt) + ' KB/s  '
    else:
        output = str(B_amt) + ' B/s   '
    return output


count = 0
try:
    while count < 15:
        req = urllib.request.Request(get_addr, data, headers)
        with urllib.request.urlopen(req) as resp:
            response = json.loads(resp.read().decode())

        output = response[rate]
        printed = convert(output)

        stdout.flush()
        stdout.write("\r%s" % printed)
        stdout.flush()
        count += 1
        sleep(2)
    stdout.write("\n")
except KeyboardInterrupt:
    stdout.write("\n")
    exit()
