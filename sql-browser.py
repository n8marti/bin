#!/usr/bin/env python3

import sqlite3
import sys
from contextlib import closing


def query_db(db_filepath, query, variables=()):
    with closing(sqlite3.connect(db_filepath)) as connection:
        with closing(connection.cursor()) as cursor:
            if variables:
                rows = cursor.execute(query, variables).fetchall()
            else:
                rows = cursor.execute(query).fetchall()
            return rows


def get_tables(db):
    names = query_db(db, "select name from sqlite_master where type='table'")
    names.sort()
    return names


def get_table_header(db, table):
    with closing(sqlite3.connect(db)) as connection:
        cursor = connection.execute(f"select * from {table}")
        return [d[0] for d in cursor.description]


def get_table_rows(db, table):
    rows = query_db(db, f"select * from {table}")
    return rows


def print_rows(rows):
    for row in rows:
        print(row)


def delete_row(db_filepath, table, column, value):
    with closing(sqlite3.connect(db_filepath)) as connection:
        with closing(connection.cursor()) as cursor:
            cursor.execute(f"delete from {table} where {column} = ?", (value,))
            connection.commit()


def main_loop(db):
    header = f"\nBrowsing {db}\nChoose action:"
    actions = {
        '1': "List tables",
        '2': "Show table data",
        '3': "Delete table row",
        'q': "Exit",
    }
    while True:
        choices = [f"{k}. {v}" for k, v in actions.items()]
        lf = '\n'
        ans = input(f"{header}\n{lf.join(choices)}\n> ")
        if ans == '1':
            tables = get_tables(db)
            for table in tables:
                print(table[0])
        elif ans == '2':
            table_name = input("Enter table name: ")
            print(get_table_header(db, table_name))
            print_rows(get_table_rows(db, table_name))
        elif ans == '3':
            table_name = input("Enter table name: ")
            column = input("Enter column header: ")
            value = input("Enter row value: ")
            delete_row(db, table_name, column, value)
        elif ans.startswith('q'):
            exit()
        else:
            print(f"Invalid choice: {ans}")
            exit(1)


def main():
    usage = "TODO: Add help text."
    if len(sys.argv) == 1:
        print(usage)
        exit(1)
    else:
        if sys.argv[1] in ['-h', '--help']:
            print(usage)
            exit(0)
        else:
            db = sys.argv[1]

    main_loop(db)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print()
        exit(1)
