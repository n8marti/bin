#!/bin/bash

script="$0"
script_name="${script##*/}"
script_dir="${script%/*}"
log="$script_dir/log/${script_name%.*}.log"

echo >> $log
echo >> $log
date >> $log
echo "----------------------------" >> $log

# Get unique ID info (UUID)
# UUID can also be found with sudo blkid
get_uuid () {
	uuid=$(ls -l /dev/disk/by-uuid | grep -v sda | grep sd* | \
		cut -d' ' -f10)
	uuid=$(lsblk -fl | grep -e 'sd[^a]' | tr -s ' ' | cut -d' ' -f3)
	# This relies on already having a dev # (e.g. /dev/sdb1)
	partition=$(ls -l /dev/disk/by-path | grep usb | \
		grep -e 'sd[a-z][0-9]' | sed -r 's@^.*/(sd[a-z][0-9])$@\1@')
	uuid=$(lsblk -fn /dev/"$partition" | tr -s ' ' | cut -d' ' -f3)
}

# Get physical description from user input
get_description () {
	read -p "Physical description: " string
	echo -e "Description:\t$string" >> $log
	echo -e "UUID:\t\t\t$uuid" >> $log
}

get_uuid
get_description

# Check if connection recognized as device by computer
usb_loc=$(dmesg | grep "USB device" | tail -n1 | cut -d' ' -f3)
dmesg_desc=$(dmesg | grep "USB device" | tail -n3 | grep "$usb_loc" \
    | head -1 | cut -d' ' -f2-)
echo -e "dmesg:\t\t\t$dmesg_desc" >> $log

# lsusb (connected & recognized as USB device)
# Check if device recognized by lsusb.
dev=$(echo "$dmesg_desc" | tr -s ' ' | cut -d' ' -f8)
lsusb=$(lsusb | grep "$dev:")
echo -e "lsusb:\t\t\t$lsusb" >> $log

# Check if recognized as storage device (e.g. /dev/sdX)
sdX=$(ls -1 /dev/sd* | grep -v sda | head -1)
if [[ $sdX ]]; then
	echo -e "sdX:\t\t\t$sdX" >> $log
else
	echo -e "sdX:\t\t\tfailed!" >> $log
fi

# Check for auto-mount as media drive
#man_mount=/media/$USER/$uuid
auto_mount=$(lsblk -fl | grep -e 'sd[^a]' | tail -n1 | tr -s ' ' | cut -d' ' -f5)

# Check read/write ability
if [[ ! $auto_mount ]]
	then
		echo "USB drive not auto-mounted."
		echo "Please mount to location below and re-run script:"
		echo "sudo mkdir -m 770 -p /media/$USER/manual"
		echo "sudo mount $sdX /media/$USER/manual"
		echo "$0"
		echo

		echo -e "auto-mount:\t\tfailed!" >> $log
		exit 10
	else
		echo -e "auto-mount:\t\t$auto_mount" >> $log
fi

# Perform write test by attempting to create folder
mkdir -p $auto_mount/test
if [[ $(ls -1 $auto_mount | grep "test") == "test" ]]
	then
		echo -e "write:\t\t\tsuccess" >> $log
		rm -d $auto_mount/test
	else
		echo -e "write:\t\t\tfailed!" >> $log
fi

if [[ $auto_mount ]]; then umount $auto_mount; fi
if [[ $(ls -1 /media/$USER | grep manual) ]]; then
	echo "Need to unmount and remove the mount location manually:"
	echo "sudo umount /media/$USER/manual"
	echo "sudo rm -R /media/$USER/manual"
	echo
fi
gedit $log &

exit 0
