#!/bin/bash


#exec 5>&1
{
idea="good"
var=$(
    # 2 outputs: &1 and &5
    #   &1 goes to the $var assignment (& zenity via tee)
    #   &5 goes to parent shell stdout (via tee)
    find /home/nate -name '*.snap' 2>/dev/null | tee >(zenity --progress --pulsate --auto-close 2>/dev/null) /dev/fd/5
    exit ${PIPESTATUS[0]}
    )
} 5>&1

echo
echo "var: $var"
echo "idea: $idea"


exit

for i in {1..9}; do
    echo "#Pass $i..."
    sleep 1
done | zenity --progress --pulsate --auto-close 2>/dev/null

exit
name="wasta [Snaps] Manager"
# Zenity defaults
ZTITLE="$name"
ZHEIGHT=200
ZWIDTH=500
ZICON=/usr/share/icons/hicolor/64x64/apps/wasta-offline.png
ZDEFAULTS=(--title="$ZTITLE" --height="$ZHEIGHT" --width="$ZWIDTH" --window-icon="$ZICON")

text="<big><b>$name</b>
Control the source and timing of snap package updates.</big>

First choose where you'd like to get updates from.

\tIf <i>Offline</i> is chosen, you will be asked to give the folder
\tlocation shortly. A wasta [Offline] folder will be automatically
\tsuggested if found.

\tIf <i>Online</i> is chosen, snap package will be downloaded from
\tthe Snap Store. Snap pacakges can potentially be hundreds of MB
\teach for the initial install, but updates only download the
\tchanges since the installed version and are smaller.

\tIf both <i>Offline</i> and <i>Online</i> are chosen <b>$name</b>
\twill first install or update snap packages from the offline source,
\tthen look for additional updates or installs from the Snap store in
\torder to minimize the amount of data downloaded.
"
list=( "False" "Offline" "wasta [Offline] or local folder" "False" "Online" "Snap store" )
zenity --list --checklist "${ZDEFAULTS[@]}" \
    --column='' --column=Source --column=Description "${list[@]}" \
    --text="$text" 2>/dev/null

exit

zenity --question --no-wrap "${ZDEFAULTS[@]}" \
        --text="<b>Use wasta [Offline] Snap Setup to add your computer's
<u>local snap cache</u> to wasta [Offline]?</b>\n

<i>You will be prompted to <b>either</b>:</i>\n
  <b>* Select</b> an <b>existing</b> <i>'wasta-offline'</i>  folder <b>or</b>\n
  <b>* Create</b> (and then <b>Select</b>) a folder of your choice.\n" 2>/dev/null

zenity --question --no-wrap "${ZDEFAULTS[@]}" \
    --text="<b>wasta-snap-manager allows snap packages to be installed
or updated both from a local and/or the online snap store.</b>

Search for snaps online?" 2>/dev/null

exit
disk=$(df /media/nate/Nates16GB/wasta-offline | tail -n1 | tr -s ' ' | cut -d ' ' -f1)
text="Not enough room left on device $disk.\n\n\
Please clear up some space and try again."
echo -e "$text"
#TODO: Needs a zenity dialog.
zenity --error "${ZDEFAULTS[@]}" --text="$text" 2>/dev/null

exit 9

prog_opts="--progress --auto-close"
for i in {0..10}; do
    prog=$(($i * 10))
    
    echo "#I would like to try some multiline text here. I don't know how it" \
          "will wrap given my current width settings.\n\nChecking variable $i..."
    echo "$prog"
    sleep 1
    ((i++))
done | zenity ${prog_opts[@]} --title="$ZTITLE" --width="$ZWIDTH" \
            --height="$ZHEIGHT" --window-icon="$ZICON" 2>/dev/null

exit

zenity --error --width=500 --height=200 --no-wrap \
    --title="wasta [Offline] Setup: Exiting" \
    --window-icon=/usr/share/icons/hicolor/64x64/apps/wasta-offline.png \
    --text="<b>Selected Folder:</b> $FOLDER_SEL\n
This folder is not a <b>'wasta-offline'</b> folder.\n
wasta [Offline] Setup will now exit.\n\n<b>No processing done...</b>" 2>/dev/null

exit

l=(pizza tacos samich speculous oreos five four three two one zero)

tmp=$(mktemp)
MAX=${#l[@]}
(
ct=1
for i in ${l[@]}; do
    prog=$(($ct*100/$MAX))
    echo -e "$prog\t$i" | tee -a "$tmp"
    ((ct += 1))
    sleep 1
done
) &
PID=$!

prog_opts="--progress --auto-close --auto-kill --no-cancel"
while [[ $(ps --no-headers -p $PID) ]]; do
    prog=$(tail -n1 "$tmp" | cut -f1)
    echo "$prog"
    text=$(tail -n1 "$tmp" | cut -f2)
    echo "#Looking at $text"
done | zenity ${prog_opts[@]} --title="Testing..." 2>/dev/null
rm "$tmp"
exit 0
