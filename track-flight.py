#!/usr/bin/env python3

import sys
from datetime import datetime
from subprocess import run

base_url = "https://www.flightstats.com/v2/flight-tracker"
airline = 'AF'
flight = '775'
t = datetime.now()
yr = t.year
mn = t.month
dt = t.day
if len(sys.argv) > 1:
    if len(sys.argv) != 3:
        print(f"Error: Need exactly two arguments: <airline> <flightno>")
        exit(1)
    airline = sys.argv[1]
    flight = sys.argv[2]

ret = run(['xdg-open', f'{base_url}/{airline}/{flight}?year={yr}&month={mn}&date={dt}'])
