#!/bin/bash

# Add a Launchpad PPA as an APT repo using HTTP instead of the default HTTPS.

# Parts needed:
#   1. the GPG signing key
#   2. the sources.list file

# Use add-apt-repository to install the HTTPS repo & key.
# Modify the sources.list file to use the HTTP address.

ppa_term=$1
source_list_file=$(
    LANG= add-apt-repository --dry-run --yes --no-update --ppa "$ppa_term" \
        | grep 'Adding deb' | awk '{print $5}'
)
if [[ -e "$source_list_file" ]]; then
    echo "INFO: Ignoring \"$ppa_term\" because file exists: $source_list_file"
    exit 1
fi
LANG= add-apt-repository --yes --no-update --ppa "$ppa_term"
sed -i 's@https@http@' "$source_list_file"
sed -i 's@launchpadcontent@launchpad@' "$source_list_file"
apt-get update
