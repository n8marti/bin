#!/bin/bash

stop=11
count=1
while [[ $count -lt $stop ]]; do
    echo "Try $count:"
    #/home/nate/scripts/tab-launch.sh &
    ping -c1 rfi.fr >/dev/null 2>&1
    rc=$?
    # Count concurrent internet connections
    ss -s | grep -E 'TCP[^:]'
    if [[ $rc != 0 ]]; then
      echo "Ping failed. Too many connections?"
    fi
    echo
    ((count += 1))
done
