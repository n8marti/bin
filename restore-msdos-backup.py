#!/usr/bin/env python3

# Takes a directory as 1st argument, which contains all backup files:
#   BACKUP.001, CONTROL.001, BACKUP.002, CONTROL.002, etc.

import pprint
import sys
from pathlib import Path


def get_filesize(bytes_list):
    p = 0
    size_b = 0
    for t in bytes_list:
        size_b += t * 256**p
        p += 1
    return size_b

def parse_header(bytes_list):
    return {
        'length': bytes_list[1],
        'signature': bytes_list[1:9],
        'sequence_number': bytes_list[9],
        'last_flag': bytes_list[-1],
    }

def parse_folder_entries(bytes_list):
    # print(f"{len(bytes_list)=}")
    if len(bytes_list) % 70 != 0:
        print(f"ERROR: Number of bytes ({len(bytes_list)}) is not a multiple of 70.")
        print(bytes_list)
        exit(1)
    num_folders = len(bytes_list) // 70
    # print(f"{num_folders=}")

    folder_entries = {}
    for d in range(num_folders):
        offset = 70 * (d - 1)
        folder_entries[d] = {
            'index': d,
            'length': bytes_list[1 + offset],
            'name': bytes_list[1+offset:1+offset+63],
            'num_entries': bytes_list[64+offset:64+offset+2],
            'end': bytes_list[66+offset:],
        }
    return folder_entries

def parse_file_entry(bytes_list):
    return {
        'length': bytes_list[1],
        'name': bytes_list[1:13],
        'type_flag': bytes_list[13],
        'size': bytes_list[14:18],
        'part': bytes_list[18:20],
        'offset': bytes_list[20:24],
        'saved_length': bytes_list[24:28],
        'attribs': bytes_list[28],
        # 'unknown': bytes_list[29],
        'timestamp': bytes_list[-4:]
    }

def parse_control_file(control_file):
    with control_file.open('r+b') as f:
        control_bytes = f.read()

    control_bytes_list = [b for b in control_bytes]
    header = control_bytes_list[:139]
    folder_entries_bytes = control_bytes_list[139:-34] # should be multiple of 70
    file_entry_bytes = control_bytes_list[-34:-1]
    end_byte = control_bytes_list[-1]
    return {
        'header': parse_header(header),
        'folder_path': parse_folder_entries(folder_entries_bytes),
        'file': parse_file_entry(file_entry_bytes),
        'end': end_byte,
    }


backup_source_dir = Path(sys.argv[1]).resolve()
stems = [
    'BACKUP',
    'CONTROL'
]
backup_source_files = [f for f in backup_source_dir.rglob('*') if f.is_file() and f.stem in stems]
backup_source_files.sort()

output_file = None
out_bytes = None
last_file = False
file_size = 0
for f in backup_source_files:
    if f.stem == 'BACKUP':
        # file_bytes = f.read_bytes()
        if not output_file:
            out_bytes = f.read_bytes()
        else:
            # print(f"Adding data to {output_file.name}...")
            with output_file.open('a+b') as o:
                o.write(f.read_bytes())
    elif f.stem == 'CONTROL':
        control_file = parse_control_file(f)
        if not output_file:
            output_filename = ''.join([chr(c) for c in control_file.get('file').get('name') if c != 0])
            folder_path = []
            for i, data in control_file.get('folder_path').items():
                folder_path.extend(data.get('name'))
            output_path_str = ''.join([chr(c) for c in folder_path if c != 0])
            output_path = Path('../', output_path_str).resolve()
            output_path.mkdir(exist_ok=True)
            output_file = output_path / output_filename
            print(f"Restoring data to: {output_file}")
        if out_bytes:
            # print(f"Writing initial data to {output_file.name}...")
            with output_file.open('w+b') as o:
                o.write(out_bytes)
                out_bytes = None
        if control_file.get('header').get('last_flag'):
            last_file = True
        if file_size == 0:
            file_size = control_file.get('file').get('size')


backup_size_b = get_filesize(file_size)
restore_size_b = output_file.stat().st_size
status = 0
if not last_file:
    print(f"ERROR: Incomplete backup file set. Restored file is invalid.")
    status = 1
if backup_size_b != restore_size_b:
    print(f"ERROR: Restored file size doesn't match backup's stated size.")
    print(f"{restore_size_b=}; {backup_size_b=}")
    status = 1
exit(status)
