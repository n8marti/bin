#!/bin/bash
# Slow script to simulate long-running process.
# usage: slow-script.sh [# of seconds to run]
declare -i end
declare -i count
end=$1
count=0
while [[ $count -lt $end ]]; do
	sleep 1
	((count++))
	echo $count
done
exit 0
