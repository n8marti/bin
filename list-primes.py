#!/usr/bin/python3

import sys


def get_given_number_of_primes(number):
    primes = [2]
    number = (int(number))
    value = 3
    while len(primes) < number:
        if check_if_prime(value):
            primes.append(value)
        value += 2
    return primes

def check_if_prime(number):
    if number == 1:
        return False
    elif number == 2:
        return True
    else:
        digit = 2
        while digit < number:
            remainder = number % digit
            if remainder == 0:
                return False
            digit += 1
        return True

def build_list_of_primes(limit):
    primes = [i for i in range(1, limit + 1) if check_if_prime(i)]
    return primes


stop = 100
if len(sys.argv) > 1:
    stop = int(sys.argv[1])
    if stop < 2:
        print("Error: You must choose a whole number greater than 1")
        exit(1)
    if len(sys.argv) > 2:
        primes = get_given_number_of_primes(sys.argv[1])
        print(f"{' '.join([str(p) for p in primes])}")
        exit()
    else:
        primes = build_list_of_primes(stop)
        if len(primes) > 1:
            s = 's'
            be = 'are'
        else:
            s = ''
            be = 'is'

        print(f"{' '.join([str(p) for p in primes])}")
        print(f"There {be} {len(primes)} prime number{s} up to {stop}")
