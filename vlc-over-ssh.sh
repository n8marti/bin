#!/bin/bash

# Play a video file from a remote host over an SSH connection.
usage="$0 [user@host] [/host's/path/to/video.mp4]"

vlc "sftp://${1}:${2}"
