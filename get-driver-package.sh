#!/bin/bash

help="Usage: $0 <driver>

Tip: List all drivers with:
sudo lshw | grep -e '*-' -e driver=
"

if [[ $1 = '-h' ]]; then
    echo "$help"
    exit 0
elif [[ $1 ]]; then
    driver="$1"
else
    echo "$help"
    exit 1
fi

# Get modinfo of driver.
modinfo=$(modinfo "$driver")
if [[ $? -ne 0 ]]; then
    # Driver not found.
    exit 1
fi

# Get filepath of driver.
filepath=$(echo "$modinfo" | grep filename | tr -s ' ' | cut -d' ' -f2)

# Get package providing driver file.
dpkg -S "$filepath" | cut -d: -f1
