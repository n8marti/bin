#!/bin/bash

# Take a hex number: 0xHHHHHHHHH
# Convert to decimal.

OUTPUT='dec'
if [[ $1 == '-h' || $1 == '--help' ]]; then
    echo "usage: $0 0xFFFFFFFFFF"
    echo "0xFFFFFFFFFF = 1099511627775"
    exit
elif [[ $1 == '-r' ]]; then
    OUTPUT='hex'
    shift
fi

if [[ $OUTPUT == 'dec' ]]; then
    hex="$1"
    lh=${#hex}
    dec=0
    exp=0
    for c in $(seq 1 $lh); do
        h=${hex:((-$c)):1}
        if [[ $h == 'x' ]]; then
            # Done with hex numbers.
            break
        elif [[ $h == 'A' || $h == 'a' ]]; then
            n=10
        elif [[ $h == 'B' || $h == 'b' ]]; then
            n=11
        elif [[ $h == 'C' || $h == 'c' ]]; then
            n=12
        elif [[ $h == 'D' || $h == 'd' ]]; then
            n=13
        elif [[ $h == 'E' || $h == 'e' ]]; then
            n=14
        elif [[ $h == 'F' || $h == 'f' ]]; then
            n=15
        else
            n=$h
        fi
        ((dec+=$((n*16**$exp))))
        ((exp+=1))
    done
    echo "$hex = $dec"
    exit
else
    dec="$1"
    q=$dec
    hex=''
    while [[ $q -ne 0 ]]; do
        r=$((q % 16))
        q=$((q / 16))
        if [[ $r -eq 10 ]]; then
            n=A
        elif [[ $r -eq 11 ]]; then
            n=B
        elif [[ $r -eq 12 ]]; then
            n=C
        elif [[ $r -eq 13 ]]; then
            n=D
        elif [[ $r -eq 14 ]]; then
            n=E
        elif [[ $r -eq 15 ]]; then
            n=F
        else
            n=$r
        fi
        hex=${n}${hex}
    done
    hex=0x${hex}
    echo "$dec = $hex"
    exit
fi
