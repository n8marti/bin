#!/usr/bin/python3
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,GObject,GdkPixbuf
from concurrent.futures import ThreadPoolExecutor
import os
 
def get_icons():
    filter = ['.png']#,'.svg']
    base_path = '/usr/share/icons'
    for (dirpath, dirnames, filenames) in os.walk(base_path):
        if not filenames: continue # If not files found just skip path.
        if '16x16' not in dirpath.lower():
            continue # limit amount as there are too many to load up.
        # Filter by extension.
        for img in filenames:
            if os.path.splitext(img)[-1].strip().lower() not in filter: 
                continue
            img_path = os.path.join(dirpath, img)
            yield img_path
 
class MyWindow(Gtk.Window):
 
    def __init__(self):
        Gtk.Window.__init__(self, title="PyCons")
        self.set_title("PyCons")
        self.set_default_size(800, 450)
 
        self.button = Gtk.Button(label="Fetch Icons")
        self.button.connect("clicked",self.fetch)
 
        self.status = Gtk.Label()
        self.status.set_text(' Idle')
        self.status.set_halign(Gtk.Align.START)
 
        self.vbox = Gtk.VBox()
 
        self.scroller = Gtk.ScrolledWindow()
        self.listbox = Gtk.ListBox()
        self.imgIcon = Gtk.Image()
 
        self.scroller.add(self.listbox)
 
        self.vbox.pack_start(self.button,False,False,5)
        self.vbox.pack_start(self.scroller,True,True,5)
        self.vbox.pack_end(self.status,False,False,5)
 
        self.add(self.vbox)
 
    def fetch(self,button):
        count=0
        with ThreadPoolExecutor(max_workers=25) as executor:
            task = executor.submit(get_icons)
            for img_path in task.result():
                count+=1
 
                pixbuf = pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(img_path,16,16) 
                icon = self.imgIcon.new_from_pixbuf(pixbuf)
 
                name = Gtk.Label()
                name.set_text(img_path.split('/')[-1].split('.')[0].strip())
 
                path = Gtk.Label()
                path.set_text(img_path)
 
                row = Gtk.ListBoxRow()
                hbox = Gtk.HBox()
                hbox.pack_start(icon,False,False,10)
                hbox.pack_start(name,False,True,10)
                hbox.pack_end(path,False,True,1)
 
                row.add(hbox)
                self.listbox.insert(row,0)
                self.listbox.show_all()
        self.status.set_text(' Fetched %s items' % count)
 
win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
