#!/bin/bash

# Info:
# https://www.lsof.pl/scripts/2016/03/07/zte-mf823-API-sending-ussd-with-curl/

addr='192.168.3.1'
user='root'
pwd='zte9x15'

host="http://$addr"

get_addr="$host/goform/goform_get_cmd_process"
post_addr="$host/goform/goform_set_cmd_process"
hhost="Host: $host"
hreferer="Referer: http://$addr/index.html"

#echo "
#\$ telnet $addr
#user: $user
#pwd:  $pwd
#"

SMS_LIST=1
sms_list="cmd=sms_data_total&page=0&data_per_page=500&mem_store=1&tags=$SMS_LIST&order_by=order+by+id+desc"

dl_rate="cmd=realtime_rx_thrpt"

post_data() {
	data="$1"
	curl $post_addr -H $hhost -H $hreferer --compressed --data $data
}

get_data() {
    data="$1"
    curl -s "$get_addr" -H "$hhost" -H "$hreferer" --compressed --data "$data"
}


#get_data "$sms_list" 2>/dev/null
#output=$(get_data "$dl_rate")
ct=0
while [[ $ct -lt 30 ]]; do
	clear
	output=$(get_data "$dl_rate")
	echo "$output B/s" #| awk '-F: {print $2 B/s}'
	((ct+=1))
	sleep 2
done
