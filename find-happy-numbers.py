#!/usr/bin/env python3

'''
Make a list of happy numbers as determined by Brad's script.
'''

import subprocess
from pathlib import Path

happy_nums_exe = Path('/home/nate/dev/happy-number/happy_nums.py')
if not happy_nums_exe.exists():
    print(f"Error: Can't find {happy_nums_exe}")
    exit(1)

happy_numbers = []
for i in range(1, 1001):
    proc = subprocess.run(
        ['python3', happy_nums_exe],
        stdout=subprocess.PIPE,
        text=True,
        input=f"{i}\n"
    )
    lines = proc.stdout.splitlines()
    conclusion = lines[-1].split()[4]
    if conclusion == 'happy':
        happy_numbers.append(i)

print(happy_numbers)
