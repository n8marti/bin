#!/bin/bash

# Demonstrate conversion to base64.

# Standard Base64 table.
BASE64=(
    "A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L" "M"
    "N" "O" "P" "Q" "R" "S" "T" "U" "V" "W" "X" "Y" "Z"
    "a" "b" "c" "d" "e" "f" "g" "h" "i" "j" "k" "l" "m"
    "n" "o" "p" "q" "r" "s" "t" "u" "v" "w" "x" "y" "z"
    "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "+" "/" "="
)

# Alternate table used for filenames and URLs.
BASE64_ALT=(
    "A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L" "M"
    "N" "O" "P" "Q" "R" "S" "T" "U" "V" "W" "X" "Y" "Z"
    "a" "b" "c" "d" "e" "f" "g" "h" "i" "j" "k" "l" "m"
    "n" "o" "p" "q" "r" "s" "t" "u" "v" "w" "x" "y" "z"
    "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "-" "_" "="
)

# Define functions.
organize_to_6bit_groups() {
    # Make groups of 6 digits.
    local regrouped=( $(echo "$1" | tr -d ' ' | sed -E 's|([01]{6})|\1 |g') )
    local padding='000000'
    local len=${#regrouped[@]}
    local last=${regrouped[$len - 1]}
    # local missing=$((6 - ${#last}))
    # Pad last group.
    regrouped[$len - 1]="$(printf "%s%s\n" "$last" "${padding:${#last}}")"
    echo "${regrouped[@]}"
}

convert_utf8_to_bits() {
    local chars="$1"
    printf "$chars" | xxd -b -c1 | awk '{print $2}' | tr '\n' ' '
}

convert_binary_to_decimal() {
    local decimals=()
    for group in $1; do
        group=( $(echo "$group" | sed -E 's|([01]{1})|\1 |g') ) # put spaces between digits
        p=5
        decimal=0
        for digit in ${group[@]}; do
            decimal=$((decimal+digit*2**p))
            p=$((p-1))
        done
        decimals+=( "$decimal" )
    done
    echo "${decimals[@]}"
}

swap_in_base64_chars() {
    local base64=
    for i in $1; do
        base64+=${TABLE[i]}
    done
    # Make groups of 4 characters.
    local base64=( $(echo "$base64" | sed -E 's|([A-Za-z0-9/+]{4})|\1 |g') )
    # padding="${BASE64[64]}${BASE64[64]}${BASE64[64]}${BASE64[64]}" # '===='
    padding="${TABLE[64]}${TABLE[64]}${TABLE[64]}${TABLE[64]}" # '===='
    local len=${#base64[@]}
    local last=${base64[$len - 1]}
    # local rem=$((len//4))
    # Pad last group.
    base64[$len - 1]="$(printf "%s%s\n" "$last" "${padding:${#last}}")"
    # Remove spaces for output.
    echo "${base64[@]}" | tr -d ' '
}

process_step() {
    local num="$1"
    local desc="$2"
    local out="$3"
    echo "Step ${num}. ${desc}:"
    echo -e "${out}\n"
    input="$out"
}

# Main processing.
TABLE=("${BASE64[@]}")
if [[ "$1" == '-u' ]]; then
    TABLE=("${BASE64_ALT[@]}")
    shift 1
fi
echo -e "Input text:\n$1"
echo

# "Text > binary" is really "UTF-8 > hex > binary"
process_step 1 "Convert text characters to 8-bit binary" "$(convert_utf8_to_bits "$1")"
process_step 2 "Regroup into 6-bit groups; pad with zeroes if necessary" "$(organize_to_6bit_groups "$input")"
process_step 3 "Convert binary to Base64 index integers" "$(convert_binary_to_decimal "$input")"
process_step 4 "Swap in Base64 characters; pad with '=' if necessary" "$(swap_in_base64_chars "$input")"
