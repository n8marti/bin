#!/usr/bin/python3

# List all "implied" Wasta dependencies
#   i.e. all ubuntu-desktop dependencies minus app-removals (and their dependencies)

import re
import subprocess


def parse_showpkg_output(pkg_output, apt_cache):
    lines = pkg_output.splitlines()
    name = lines[0].strip()
    deps = []
    rdeps = []
    d = 0
    r = 0
    for line in lines:
        if r == 1:
            if line[0] == ' ':
                rdeps.append(line.strip().split(',')[0])
                continue
            else:
                # End of indented list.
                r = 0
        if d == 1:
            if line != 'Provides: ':
                # get rid of anything between parentheses
                line = re.sub(' \([0-9]{1,2} [\(0-9a-z\):.+~-]+\) ', ' ', line)
                # get rid of '- '
                line = re.sub(' - ', ' ', line)
                deps.extend(line.split()[1:])
                continue
            else:
                # End of list.
                d = 0
        mr = re.match('^Reverse Depends:.*$', line)
        md = re.match('^Dependencies:.*$', line)
        if mr:
            r = 1
            continue
        if md:
            d = 1
            continue
    rdeps = list(set(rdeps))
    rdeps.sort()
    deps = list(set(deps))
    deps.sort()
    apt_cache[name] = [rdeps, deps]
    return apt_cache

def get_rdepends(pkg):
    cmd = ["apt-cache", "rdepends", pkg]
    run = subprocess.run(cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    output_line_list = run.stdout.decode().splitlines()[2:]
    rdeps = []
    for i in output_line_list:
        rdep = i.strip()
        rdeps.append(rdep)
    return rdeps

# ------------------------------------------------------------------------------
# Gather package info for ubuntu-desktop dependencies and app-removals.sh
# ------------------------------------------------------------------------------
# Get list of packages to remove from app-removals.sh.
cmd_grep = subprocess.run(
    ["grep", "-A59", "pkgToRemoveListFull=\"",
    "/usr/share/wasta-core/scripts/app-removals.sh"],
    stderr=subprocess.STDOUT,
    stdout=subprocess.PIPE
    )
output_line_list = cmd_grep.stdout.decode().splitlines()
pkg_list=output_line_list[1:60]
remove_list = []
for p in pkg_list:
    remove_list.append(p.rstrip('\\"').strip())
remove_list.sort()

# Get list of ubuntu-desktop dependencies.
# Build "apt-cache"-like database for all ubuntu-desktop packages.
cmd = ["apt-cache", "showpkg"]
run = [*cmd, "ubuntu-desktop"]
cmd_cache = subprocess.run(run, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
output_list = cmd_cache.stdout.decode().split('Package:')[1:]
ubuntu_desktop_proper_cache = {}
for i in output_list:
    ubuntu_desktop_proper_cache = parse_showpkg_output(i, ubuntu_desktop_proper_cache)
ubuntu_desktop_deps = [v[1] for v in ubuntu_desktop_proper_cache.values()][0]

run = [*cmd, *ubuntu_desktop_deps]
cmd_cache = subprocess.run(run, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
output_list = cmd_cache.stdout.decode().split('Package:')[1:]
ubuntu_desktop_cache = {}
for i in output_list:
    ubuntu_desktop_cache = parse_showpkg_output(i, ubuntu_desktop_cache)
ubuntu_desktop_proper_deps = list(ubuntu_desktop_cache.keys())
# List dependencies of each package in ubuntu-desktop.
ubuntu_deps = []
for v in ubuntu_desktop_cache.values():
    ubuntu_deps.extend(v[1])
ubuntu_deps = list(set(ubuntu_deps))
ubuntu_deps.sort()

# Build "apt-cache"-like database for wasta-removed packages.
cmd = ["apt-cache", "showpkg"]
run = [*cmd, *remove_list]
cmd_cache = subprocess.run(run, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
output_list = cmd_cache.stdout.decode().split('Package:')[1:]
rm_cache = {}
for i in output_list:
    rm_cache = parse_showpkg_output(i, rm_cache)


# ------------------------------------------------------------------------------
# Build lists of "true" wasta depenencies and other non-ubuntu-desktop removals.
# ------------------------------------------------------------------------------
wasta_desktop_cache = ubuntu_desktop_cache.copy()
rm_cache_copy = rm_cache.copy()
print("Comparing packages in app-removals.sh with packages in ubuntu-desktop...")
for pkg in rm_cache.keys():
    # Remove any packages explicitly listed in ubuntu-desktop dependencies.
    if pkg in ubuntu_desktop_cache.keys():
        wasta_desktop_cache.pop(pkg, None)
        rm_cache_copy.pop(pkg, None)
        continue
    for rdep in rm_cache[pkg][0]:
        # Remove any packages whose rdepends are listed in ubuntu-desktop deps.
        if rdep in ubuntu_desktop_cache.keys():
            wasta_desktop_cache.pop(rdep, None)
            rm_cache_copy.pop(pkg, None)
            break
        # Remove any whose rdepends are listed as a dependency to any ubuntu-desktop pkgs.
        if rdep in ubuntu_deps:
            wasta_desktop_cache.pop(rdep, None)
            rm_cache_copy.pop(pkg, None)
            break
        # Remove any whose "parent-rdepends" are listed as a dep to any ubuntu-desktop pkgs.
        p_rdeps = get_rdepends(rdep)
        for p_rdep in p_rdeps:
            print(".", end='', flush=True)
            if p_rdep in ubuntu_deps:
                wasta_desktop_cache.pop(p_rdep, None)
                rm_cache_copy.pop(pkg, None)
                print()
                break
print("\n")

# Print final summary.
ctu = str(len(ubuntu_desktop_proper_deps))
cta = str(len(remove_list))
ctw = str(len(wasta_desktop_cache.keys()))
ctr = str(len(rm_cache_copy.keys()))
print("No. of ubuntu-desktop dependencies:", ctu)
print("No. of packages removed by app-removals.sh:", cta)
print()
print("wasta-desktop dependencies [" + ctw + "]:")
for i in wasta_desktop_cache.keys():
    print(" "*3 + i)
print()
print("Packages removed by wasta without dependencies in ubuntu-desktop [" + ctr + "]:")
for i in rm_cache_copy.keys():
    print(" "*3 + i)
exit()
