#!/bin/bash

# Expect $1 as device, partition, or IMG file.
# Expect $2 as destination file path.
# Use xz -0 for fastest compression:
#   - still better than gzip
#   - gnome-disks can flash IMG.XZ files straight to devices with first decompressing
# Accept other argument for compression level?


handle_dev_input() {
    # Ensure outfile_path is given.
    if [[ -z "$2" ]]; then
        echo "ERROR: No outfile path given."
        return 1
    fi

    # Ensure outfile_path is writeable.
    out_dir=$(dirname "$2")
    if [[ ! -d "$out_dir" ]]; then
        echo "ERROR: \"$out_dir\" does not exist."
        return 1
    elif [[ ! -w "$out_dir" ]]; then
        echo "ERROR: \"$out_dir\" is not writeable."
        return 1
    fi

    # Ensure user ownership of compressed outfile.
    touch "$2"
    if [[ "$SUDO_USER" ]]; then
        chown $SUDO_USER:$SUDO_USER "$2"
    fi

    # Unmount partitions and calculate device size for showing compression progress.
    if [[ -b "$1" ]]; then
        # Input device is truly a device.
        if [ $(id -u) -ne 0 ]; then
            echo "ERROR: Please run script again with root privileges."
            return 1
        fi
        mount_points=$(lsblk | grep $(basename "$1") | grep part | awk '{print $7}')
        while IFS= read -r mp; do
            umount "$mp"
        done <<< "$mount_points"

        # Calculate device size.
        kibibytes=$(cat /proc/partitions | grep "$(basename $1)$" | awk '{print $3}')
    else
        echo "ERROR: \"$1\" not a valid device."
        return 1
    fi

    # Create compressed image of device.
    if [[ -z "$kibibytes" ]]; then
        echo "Can't caculate device size; only showing activity instead of progress."
        dd bs=128K conv=noerror,sync if="$1" | pv | xz -0 --threads=0 > "$2"
    else
        dd bs=128K conv=noerror,sync if="$1" | pv -s "$kibibytes"K | xz -0 --threads=0 > "$2"
    fi
}

handle_img_input() {
    if [[ -n "$2" ]]; then
        echo "ERROR: Explicit outfile path for compressing an image not yet supported."
        return 1
    fi
    xz -0 --keep --verbose --threads=0 "$1"
}

handle_xz_input() {
    if [[ $(file "$1" | awk -F':' '{print $2}' | awk '{print $1}') == 'XZ' ]]; then
        echo "Input file already xz-compressed."
        return 0
    else
        echo "ERROR: Input file has xz extension but is not xz-compressed."
        return 1
    fi
}

handle_zip_input() {
    # Verify ZIP format.
    if [[ $(file "$1" | awk -F':' '{print $2}' | awk '{print $1}') != 'Zip' ]]; then
        echo "ERROR: Input file has zip extension but is not zip-compressed."
        return 1
    fi

    # Recompress as XZ.
    outfile=${1%.*}.xz
    length=$(unzip -l "$1" | tail -n1 | awk '{print $1}')
    unzip -p "$1" | pv -s "$length" | xz -0 --threads=0 > "$outfile"
}


debug=
usage="Usage:
  $0 /dev/DEVICE /path/to/outfile.img.xz
  $0 /path/to/image.img [/path/to/outfile.img.xz]
  $0 /path/to/image.img.zip [/path/to/outfile.img.xz]"
help_text="$usage

  -d    Run in debug (set -x) mode.
  -h    Show this help and exit."

while getopts ":dh" opt; do
    case $opt in
        d) # debug
            debug=yes
            ;;
        h) # help text
            echo "$help_text"
            exit 0
            ;;
        *) # invalid option
            echo "$help_text"
            exit 1
            ;;
    esac
done
shift $(($OPTIND - 1))

# Check for DEBUG mode.
if [[ -n "$debug" ]]; then
    set -x
fi

# Set main variables.
input_text="$1"
outfile_path="$2"

# Ensure input argument.
if [[ -z "$input_text" ]]; then
    echo "ERROR: No input device or image path given."
    echo "$usage"
    exit 1
fi

# Determine input_type.
input_type="${input_text##*.}"
if [[ $(dirname "$input_type") == '/dev' ]]; then
    input_type='dev'
fi

# Ensure input is readable.
if [[ ! -r "$input_text" ]]; then
    echo "ERROR: \"$input_text\" not a readable device or file."
    exit 1
fi

# Process input file based on file type.
case "$input_type" in
    'img') # image file
        handle_img_input "$input_text" "$outfile_path"
        exit $?
        ;;
    'xz') # xz file
        handle_xz_input "$input_text"
        exit $?
        ;;
    'zip') # zipped image file
        handle_zip_input "$input_text"
        exit $?
        ;;
    'dev') # raw device
        handle_dev_input "$input_text" "$outfile_path"
        exit $?
        ;;
    *) # unsupported type
        echo "ERROR: Extension \"$input_type\" not supported."
        exit 1
        ;;
esac
