#!/bin/bash

# Ref:
#   - https://developer.android.com/studio/command-line/logcat

day=$(date +%Y-%m-%d)
LOG="$HOME/moto-mobile-connection-${day}.log"
echo "Saving output to: $LOG"
echo

# Limit output to radios only:
#   -b radio
# Filter by given tag:
#   MtkDC-1, MtkConnectivityService
# Give debug/verbose output of given tag:
#   <tag>:D/<tag>:V
# Silence all other output:
#   *:S
adb logcat \
    --buffer=all \
    --format=long \
    --format=color \
    MtkDC-1:V \
    MtkConnectivityService:V \
    *:S \
    | tee -a "$LOG"
