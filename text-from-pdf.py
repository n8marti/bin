#!/usr/bin/env python3

# import PyPDF2
import pdfplumber
import sys

infile = sys.argv[1]
# with open(infile, 'rb') as f:
#     pdfr = PyPDF2.PdfFileReader(f)
#     pg_1 = pdfr.getPage(0)
#     print(pg_1.extractText())

with pdfplumber.open(infile) as pdf:
    print(pdf.metadata)
    print(pdf.pages[0].extract_text(layout=True))
    # print(pdf.pages[0].chars[100])
    im = pdf.pages[0].to_image(resolution=150)
    im.show()
