#!/bin/bash

# NOTE: Metadata editing can be done via a TSV file with:
#   $ ffmpeg-update-metadata.sh -f TSV_FILE INFILE
#
#   $ # Officially, it's possible with an INI-style file, but 
#   $ #     this seems to be unreliable when multiple streams are involved.
#   http://ffmpeg.org/ffmpeg-formats.html#Metadata-1
#   $ # Generate file with current metadata.
#   $ ffmpeg -i INPUT -f ffmetadata FFMETADATAFILE
#   $ # OR
#   $ ffmpeg -i INPUT -c copy -map_metadata 0 -map_metadata:s:v 0:s:v -map_metadata:s:a 0:s:a -map_metadata:s:s 0:s:s -f ffmetadata in.txt
#   $ # [Update the text file's data...]
#   $ # Write the updated file's data into the media file.
#   $ ffmpeg -i INPUT -i FFMETADATAFILE -map_metadata 1 -codec copy OUTPUT # 


function run_command() {
    infile="$1"
    outfile="${infile%.*}_m.mp4"

    # Ensure that command arguments are valid.
    if [[ ${#args} -lt 1 ]]; then
        echo "Noting to update."
        exit 0
    elif [[ ! -r "$infile" ]]; then
        echo "Input file not valid: $infile"
        exit 1
    elif [[ -z "$outfile" ]]; then
        echo "No output file given."
        exit 1
    fi

    if [[ "$verbose" -eq 1 ]]; then
        echo "arg:" "${args[@]}"
        echo "cmd: ffmpeg -i $infile -map 0 -c copy" "${args[@]}" "$outfile"
    fi
    ffmpeg -hide_banner -i "$infile" -map 0 -c copy "${args[@]}" "$outfile"
}

help_text="Usage:\n  $0 [-t TITLE] [-a AUDIO_STREAM_INDEX | -s SUBS_STREAM_INDEX [-i LANG_ISO] [-l LABEL]] INFILE\n  $0 -t TSV_FILE INFILE"
while getopts ":a:f:hi:l:s:t:v" opt; do
    case $opt in
        a)
            audio_stream_index="$OPTARG"
            ;;
        f)
            tsv_file="$OPTARG"
            ;;
        h)
            echo -e "$help_text"
            exit 0
            ;;
        i)
            iso="$OPTARG"
            ;;
        l)
            label="$OPTARG"
            ;;
        s)
            subs_stream_index="$OPTARG"
            ;;
        t)
            title="$OPTARG"
            ;;
        v)
            verbose=1
            ;;
        *) # invalid option
            echo -e "$help_text"
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

args=()
if [[ -f "$tsv_file" ]]; then
    ct=0
    while read -r l; do
        ((ct+=1))
        if [[ $ct -eq 1 || $l =~ ^$ || $l =~ ^# ]]; then
            echo "Line skipped: $l"
            continue
        fi
        # Add data to command args.
        stream="$(echo "$l" | awk -F'\t' '{print $1}')"
        type="$(echo "$l" | awk -F'\t' '{print $2}')"
        data="$(echo "$l" | awk -F'\t' '{print $3}')"
        args+=( "-metadata:${stream}" "${type}=${data}" )
    done < "$tsv_file"
    run_command "$1"
    exit $?
fi

# Update title.
if [[ -n "$title" ]]; then
    args+=( -metadata:g title="$title" )
fi
# Verify that a stream index is passed before continuing.
if [[ -n "$iso" || -n "$label" ]]; then
    if [[ -z "$audio_stream_index" && -z "$subs_stream_index" ]]; then
        echo "Updating language ISO code and/or label requires an audio or subtitle stream index (-a or -s)."
        exit 1
    fi
    if [[ -n "$label" ]]; then
        # Ensure that label has no spaces.
        label="$(echo "$label" | tr ' ' '_')"
        # if [[ $(echo "$label" | tr -d ' ') != "$label" ]]; then
        #     echo "Error: LABEL cannot contain spaces."
        #     exit 1
        # fi
    fi
fi
# Update audio stream metadata.
if [[ -n "$audio_stream_index" ]]; then
    if [[ -n "$iso" ]]; then
        args+=( -metadata:s:a:"$audio_stream_index" language="$iso" )
    fi
    if [[ -n "$label" ]]; then
        args+=( -metadata:s:a:"$audio_stream_index" title="$label" )
    fi
# Update subtitles stream metadata.
elif [[ -n "$subs_stream_index" ]]; then
    if [[ -n "$iso" ]]; then
        args+=( -metadata:s:s:"$subs_stream_index" language="$iso" )
    fi
    if [[ -n "$label" ]]; then
        args+=( -metadata:s:s:"$subs_stream_index" title="$label" )
    fi
fi

run_command "$1"

# ffmpeg -i "$infile" -map 0 -c copy \

#     -metadata:g title="JESUS" \
#     -metadata:s:a:0 language=liy \
#     -metadata:s:a:0 title="banda-linda" \
#     -metadata:s:a:1 language=bdt \
#     -metadata:s:a:1 title="bhogoto" \
#     -metadata:s:a:2 language=gbv \
#     -metadata:s:a:2 title="gbagiri" \
#     -metadata:s:a:3 language=gso \
#     -metadata:s:a:3 title="gbaya SW" \
#     -metadata:s:a:4 language=ksp \
#     -metadata:s:a:4 title="kabba" \
#     -metadata:s:a:5 language=mzv \
#     -metadata:s:a:5 title="mandja" \
#     -metadata:s:a:6 language=mcx \
#     -metadata:s:a:6 title="mpyemo" \
#     -metadata:s:a:7 language=lnl \
#     -metadata:s:a:7 title="ngbugu" \
#     -metadata:s:a:8 language=nzk \
#     -metadata:s:a:8 title="nzakara" \
#     -metadata:s:a:9 language=sag \
#     -metadata:s:a:9 title="sango" \
#     -metadata:s:a:10 language=axk \
#     -metadata:s:a:10 title="yaka" \

#     -metadata:s:s:0 language=liy \
#     -metadata:s:s:0 title="Banda-Linda" \
#     -metadata:s:s:1 language=bdt \
#     -metadata:s:s:1 title="bhogoto" \
#     -metadata:s:s:2 language=gbv \
#     -metadata:s:s:2 title="gbagiri" \
#     -metadata:s:s:3 language=gso \
#     -metadata:s:s:3 title="gbaya SW" \
#     -metadata:s:s:4 language=ksp \
#     -metadata:s:s:4 title="kabba" \
#     -metadata:s:s:5 language=mzv \
#     -metadata:s:s:5 title="mandja" \
#     -metadata:s:s:6 language=mcx \
#     -metadata:s:s:7 title="mpyemo" \
#     -metadata:s:s:7 language=lnl \
#     -metadata:s:s:7 title="ngbugu" \
#     -metadata:s:s:8 language=nzk \
#     -metadata:s:s:8 title="nzakara" \
#     -metadata:s:s:9 language=sag \
#     -metadata:s:s:9 title="sango" \
#     -metadata:s:s:10 language=axk \
#     -metadata:s:s:10 title="yaka" \

#     "$outfile"
