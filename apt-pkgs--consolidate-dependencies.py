#!/usr/bin/python3

# - Accept input file as package list.
# - Reduce the package list to core reverse-depends that include all other listed packages.

import re
import subprocess
import sys

script_relpath = sys.argv[0]
infile = sys.argv[1]

try:
    debug = True if sys.argv[2] == 'debug' else False
except IndexError:
    debug = False


def parse_showpkg_output(pkg_output, apt_cache):
    sections = re.split(r'(\n[a-z]+[ ]*[a-z]*:)', pkg_output, flags=re.IGNORECASE)
    for i in range(len(sections)):
        if i == 0:
            name = sections[i].strip()
            apt_cache[name] = {}
        elif i % 2 == 1: # odd index
            section = sections[i].strip('\n').strip(':')
        elif i % 2 == 0: # even index
            content = sections[i]
            if section == 'Versions':
                ver_top = content.split()[0]
                apt_cache[name][section] = ver_top
            elif section == 'Reverse Depends':
                rdeps = []
                rdeplines = content.splitlines()
                for rdepline in rdeplines:
                    try:
                        ver_min = rdepline.strip().split()[1]
                    except IndexError:
                        ver_min = 'any'
                    if ver_min == 'any' or ver_min <= ver_top:
                        dep = rdepline.strip().split(',')[0]
                        if dep:
                            rdeps.append(dep)
                rdeps = list(set(rdeps))
                rdeps.sort()
                apt_cache[name][section] = rdeps
            elif section == 'Dependencies':
                deps = []
                dep_items = {}
                pat = r'(\n[0-9a-z\.\:\-\+~]+ - )'
                ver_content = re.split(pat, content, flags=re.IGNORECASE)
                ver_content = [i for i in ver_content if i != ' ']
                for i in range(len(ver_content)):
                    if i % 2 == 0:
                        pat = r'\s([0-9a-z\.\:\-\+~]+) - '
                        ver_dep = re.sub(pat, r'\1', ver_content[i], flags=re.IGNORECASE)
                    elif i % 2 == 1:
                        dep_content = ver_content[i]
                        dep_items[ver_dep] = dep_content
                for ver, value in dep_items.items():
                    pat = r'\([0-9]+ [0-9a-z\(\)\.:\-nul\+~]+\)'
                    ver_deps = re.split(pat, value, flags=re.IGNORECASE)
                    ver_deps = [i.strip() for i in ver_deps if i != ' ']
                    if ver == ver_top:
                        deps = ver_deps
                        #deps.extend(ver_deps)
                deps.sort()
                apt_cache[name][section] = deps
    return apt_cache

def create_cache_from_list(pkgs_list):
    cmd = ["apt-cache", "showpkg"]
    run = [*cmd, *pkgs_list]
    cmd_cache = subprocess.run(run, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    output_list = cmd_cache.stdout.decode().split('Package:')[1:]
    # pkg_cache[name] = {'Versions': ver_top, 'Reverse Depends': [*rdeps], 'Dependencies': [*deps]}
    pkgs_cache = {}
    for i in output_list:
        pkgs_cache = parse_showpkg_output(i, pkgs_cache)
    return pkgs_cache

def get_rdepends(pkg):
    cmd = ["apt-cache", "rdepends", pkg]
    run = subprocess.run(cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    output_line_list = run.stdout.decode().splitlines()
    rdeps = []
    for i in output_line_list:
        if i[0] != ' ':
            continue
        rdep = i.strip()
        rdeps.append(rdep)
    return rdeps

def print_summary(pkgs_removed, pkgs_to_install_cache):
    if pkgs_removed:
        print(f"{len(pkgs_removed)} packages")
    print(f"{len(pkgs_to_install_cache)} independent packages\n")
    return


# ------------------------------------------------------------------------------
# Create 'apt-cache'-like database from input file packages list.
# ------------------------------------------------------------------------------
# Slurp infile data into list. Assume input file has line-separated list.
with open(infile, 'r') as f:
    # List items include newlines.
    pkgs_wanted = f.readlines()
# Strip newlines from list items.
pkgs_wanted_list = [i.strip() for i in pkgs_wanted]
# Create 'apt-cache'-like database from packages list.
pkgs_wanted_cache = create_cache_from_list(pkgs_wanted_list)


# ------------------------------------------------------------------------------
# Whittle down the list to only foundational packages.
# ------------------------------------------------------------------------------
pkgs_to_install_cache = pkgs_wanted_cache.copy()
if debug:
    print_summary(None, pkgs_to_install_cache)

removed = []
backup = pkgs_to_install_cache.copy()
for pkg in backup:
    rdeps = backup[pkg]['Reverse Depends']
    # How to deal with this case?:
    #   - rdep in original list and in package's deps (pair) [fieldworks],
    #       but at least one of the pair has another rdep in original list: remove package
    #
    # fieldworks-applications
    #  L fieldworks
    #     L fieldworks-applications
    #
    #  fieldworks-applications
    #   L flexbridge
    #      L fieldworks-applications
    #
    # Somehow I need to remove fieldworks and flexbridge but not fieldworks-applications.
    #   How are fieldworks and flexbridge different from fieldworks-applications?
    #   - they have relatively few rdeps and deps
    #   - they only have 1 rdep that is also a dep (f-a has 2)
    codeps = []
    for rdep in rdeps:
        # Test if rdep is also a dep.
        try:
            deps = backup[pkg]['Dependencies']
        except KeyError:
            deps = []
        # Only remove pkg if it has an rdep in the list that is not also a dep.
        #   If the rdep is also a dep and you remove pkg, you will also end up
        #   removing the rdep later, and both will be gone (e.g. gedit[-common]).
        if rdep in backup.keys():
            if rdep in deps:
                codeps.append(rdep)
            else:
                pkgs_to_install_cache.pop(pkg, None)
                removed.append(pkg)
            if len(codeps) > 1:
                for codep in codeps:
                    pkgs_to_install_cache.pop(codep, None)
                    removed.append(codep)

removed = list(set(removed))
if debug:
    print("Checked if packages' reverse depends are among those provided:")
    print_summary(removed, pkgs_to_install_cache)
if not debug:
    for k in pkgs_to_install_cache.keys():
        print(k)

exit()
# List dependencies of each given package.
deps_of_pgks_wanted = {}
for pkg, values in pkgs_wanted_cache.items():
    for dep in values['deps']:
        try:
            deps_of_pgks_wanted[dep].append(pkg)
        except KeyError:
            deps_of_pgks_wanted[dep] = [pkg]

# Remove any package that is a dependency of another package in the list.
#   Some packages are among each other's dependencies (gedit, gedit-common), so
#   need to check if dependency has already been removed before removing pkg.
removed = []
for pkg in pkgs_to_install_cache.copy().keys():
    if pkg in deps_of_pgks_wanted.keys():
        rdeps = deps_of_pgks_wanted[pkg]
        all_removed = 0
        for rdep in rdeps:
            if rdep in removed:
                all_removed = 1
        if all_removed == 0:
            if re.match('gedit.*', pkg) is not None:
                print(f"{pkg} removed because it's a dependency of {deps_of_pgks_wanted[pkg]}")
            pkgs_to_install_cache.pop(pkg, None)
            removed.append(pkg)
removed = list(set(removed))
if debug:
    print("Checked if packages are dependencies of other packages:")
    print_summary(removed, pkgs_to_install_cache)

# Remove any package whose rdepends are also in the list.
#   Some packages are among each other's rdepends (gedit, gedit-common), so
#   need to check if rdep has already been removed before removing pkg.
for pkg, values in pkgs_to_install_cache.copy().items():
    for rdep in values['Reverse Depends']:
        if rdep in pkgs_wanted_cache.keys() and rdep not in removed:
            pkgs_to_install_cache.pop(pkg, None)
            removed.append(pkg)
removed = list(set(removed))
if debug:
    print("Checked if packages have rdepends in the list:")
    print_summary(removed, pkgs_to_install_cache)

# List parent rdeps of all packages in the list.
all_parent_rdeps = []
for pkg, values in pkgs_to_install_cache.items():
    all_parent_rdeps.extend(values['Dependencies'])
all_parent_rdeps = list(set(all_parent_rdeps))

# Remove any package whose "parent-rdepends" are listed as a dependency to any package in the list.
for pkg, values in pkgs_to_install_cache.copy().items():
    if pkg in all_parent_rdeps:
        pkgs_to_install_cache.pop(pkg, None)
        removed.append(pkg)
removed = list(set(removed))
if debug:
    print("Checked if packages have parent-rdepends that are dependencies of another package in the list:")
    print_summary(removed, pkgs_to_install_cache)

if debug:
    exit()
else:
    for k in pkgs_to_install_cache.keys():
        print(k)
