#!/bin/bash

tmp_pipe="/tmp/zenity-fifo"
if [[ ! -p $tmp_pipe ]]; then
    mkfifo "$tmp_pipe"
fi
#exec 5<> "$tmp_pipe"

#input="Initial text"
#read -u5 input; do

#{
#    sleep 2
#    echo "#$input"
#    sleep 2
#} | zenity --progress --pulsate --text="TestText" #<"$tmp_pipe"
while true; do
    echo "- stdout -"
done | zenity --progress --pulsate --text="TestText" <"$tmp_pipe" &

input="#TestInput"
echo "$input" >"$tmp_pipe"

#if [[ $m_handling == '@' ]]; then
#    # Command had no output. Pipe is now empty.
#    m_handling=null
#else
#    # Command output was read into $m_handling, now remove '@' from pipe.
#    read -u5 ignored
#fi


# Remove temporary pipe.
#exec 5>&-
rm "$tmp_pipe"
