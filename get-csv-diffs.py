#!/usr/bin/env python3

import csv
import sys

if len(sys.argv) < 3 or '-h' in sys.argv:
    print(f"Usage: {sys.argv[0]} COMPFILE.CSV BASEFILE.CSV")
    print("Lines in COMPFILE not in BASEFILE will be printed to stdout.")
    exit(1)

basefilepath = sys.argv[1]
compfilepath = sys.argv[2]

with open(compfilepath) as cf:
    cfreader = csv.reader(cf, delimiter=';')
    compfilerows = [r for r in cfreader]

extranames = [r[0] for r in compfilerows]
with open(basefilepath) as bf:
    bfreader = csv.reader(bf, delimiter=';')
    baselines = 0
    for row in bfreader:
        baselines += 1
        # print(f"Checking {row[0]=}")
        try:
            extranames.remove(row[0])
            # print(f"BASEFILE row {baselines} found in COMPFILE.")
            # print(f"{len(extranames)=}")
        except ValueError:
            err = f"BASEFILE \"{row[0]}\" NOT found in COMPFILE."
            if ',' in row[0]:
                ln, fn = row[0].split(',')
                ln = ln.strip()
                fn = fn.strip()
                fullname = f"{fn} {ln}"
                try:
                    err = f"BASEFILE \"{fullname}\" NOT found in COMPFILE." 
                    extranames.remove(fullname)
                except:
                    print(err, file=sys.stderr)
            else:
                print(err, file=sys.stderr)

for n in extranames:
    print(n)

print(f"# of lines in COMPFILE: {len(compfilerows)}", file=sys.stderr)
print(f"# of lines in BASEFILE: {baselines}", file=sys.stderr)
print(f"# of extra lines found in COMPFILE: {len(extranames)}", file=sys.stderr)

