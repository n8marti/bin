#!/usr/bin/python3

"""See if any packages in given input list (needles) are found in list in question (haystack).
"""

# WIP
print("Work in progress.")
exit()
import sys


def normalize_plain_text_list(plain_text_list):
    data = {
        'pkgs': []
    }

    with open(plain_text_list) as f:
        lines = f.readlines()
    for l in lines:
        data['pkgs'].append(l.split()[0])
    return data


def normalize_apt_depends_list(apt_depends_list):
    data = {
        'depends': {},
        'recommends': {},
        'lost_deps': [],
        'lost_recs': [],
        'lost_items': [],
    }

    with open(apt_depends_list) as f:
        lines = f.readlines()

    pkg = None
    type = None
    for l in lines:
        l = l.rstrip()
        if l[0] != ' ':
            # package name
            pkg = l
            data['depends'][pkg] = []
            type = None
        elif l[2:9] == 'Depends':
            # dependency
            type = 'Depends'
            dep = l.split(':')[1].strip().lstrip('<').rstrip('>')
            if pkg:
                data['depends'][pkg].append(dep)
            else:
                data['lost_deps'].append(dep)
        elif l[2:12] == 'Recommends':
            # recommends
            type = 'Recommends'
            rec = l.split(':')[1].strip().lstrip('<').rstrip('>')
            if pkg:
                data['recommends'][pkg].append(rec)
            else:
                data['lost_recs'].append(rec)
        elif l[2:10] == 'Suggests':
            pass
        else:
            # continuing list from prev line
            item = l.strip().lstrip('<').rstrip('>')
            if type == 'Depends':
                if pkg:
                    data['depends'][pkg].append(item)
                else:
                    data['lost_deps'].append(item)
            elif type == 'Recommends':
                if pkg:
                    data['recommends'][pkg].append(item)
                else:
                    data['lost_recs'].append(item)
            else:
                data['lost_items'].append(item)
    return data

def id_list_format(infile):
    file_type = 'plain-text'
    with open(infile) as f:
        lines = f.readlines()
    print(lines[1][2:10])
    if lines[1][2:10] == 'Depends:':
        file_type = 'apt-cache-depends'
    return file_type

args = sys.argv[:]
usage = f'Usage: {sys.argv[0]} NEEDLES_LIST HAYSTACK_LIST'
desc = f'\n   List items in input files should be separated by newlines.\n   Any files generated from APT should use LANG=C env variable.'
if args[1] == '-h' or args[1] == '--help' or len(args) != 3:
    print(f'{usage}\n{desc}')
    exit()

infile = sys.argv[1]
sfile = sys.argv[2]

infile_fmt = id_list_format(infile)
sfile_fmt = id_list_format(sfile)
print(infile_fmt)

if infile_fmt == 'apt-cache-depends':
    needles_data = normalize_apt_depends_list(infile)
else:
    needles_data = normalize_plain_text_list(infile)

haystack_data = normalize_plain_text_list(sfile)
print(needles_data)
