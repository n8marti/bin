#!/bin/bash

time=$(date +%F-%H:%M:%S)
#echo "Time and date stamp: $time"
location=/$HOME/bandwidth-usage
mkdir -p "$location"
output_file="$location/bandwidth_usage_$time.png"
#echo "Output file: $output_file"

vnstati -i wlp1s0 -d -o "$output_file"
eog -n "$output_file"

exit 0
