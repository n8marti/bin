#!/bin/bash

# Take images from a source folder and add them using the given framerate.

framerate=10
usage="$0 [-f FRAMERATE($framerate)] /PATH/TO/SOURCE/IMG/DIR"

# Handle arguments.
while getopts ":f:h" opt; do
    case $opt in
        f) # framerate
            framerate="$OPTARG"
            ;;
        h) # help text
            echo "$usage"
            exit 0
            ;;
        *) # invalid option
            echo "Error: \"-$OPTARG\" is invalid."
            echo "$usage"
            exit 1
            ;;
    esac
done
shift $(($OPTIND - 1))

# Set source directory.
if [[ -z "$1" ]]; then
    echo "Error: Need to give directory of source images."
    echo "$usage"
    exit 1
elif [[ ! -d "$1" ]]; then
    echo "Error: \"$1\" is not a directory."
    echo "$usage"
    exit 1
else
    source_dir=$(realpath "$1")
    name=$(basename "$source_dir")
fi

ffmpeg -framerate "$framerate" -pattern_type 'glob' -i "${source_dir}/*.jpg" \
    -c:v 'libx264' -pix_fmt 'yuv420p' "${source_dir}/${name}.mp4"
