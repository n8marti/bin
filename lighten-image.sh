#!/bin/bash

# Bad image scan (PDF or JPG)? Lighten and sharpen it a little.
LANG=C

# Set DPI and output size.
dpi=150
w_mm=210
h_mm=297

# Do conversions.
dpmm=$(echo "scale=3; ${dpi}/25.4" | bc) # dots per mm
# Convert dims in pixels as floats.
w_px_float=$(echo "scale=3; ${dpmm}*${w_mm}" | bc)
h_px_float=$(echo "scale=3; ${dpmm}*${h_mm}" | bc)
# Convert floats to integers.
printf -v w_px %.0f "$w_px_float"
printf -v h_px %.0f "$h_px_float"

# Set final dims.
dims="${w_px}x${h_px}"

# Set static options.
in_opts=" -density $dpi "
out_opts=" -resize $dims -sharpen 0x.5 "

# Set dynamic options.
if [[ "$1" == 'c' ]]; then
    m="$1"
    color_opts=" -brightness-contrast 5x25 "
elif [[ "$1" == 'g' ]]; then
    m="$1"
    color_opts=" -colorspace gray -normalize -level 50%,51% "
elif [[ -z "$1" || "$1" == 'b' ]]; then
    m="b"
    color_opts=" -monochrome -normalize -level 50%,51% "
fi

# Convert files.
for t in ".jpg" ".pdf"; do
    echo "Converting $t files..."
    find . -maxdepth 1 -name "*$t" -type f -exec \
        convert \
            $in_opts \
            {} \
            $color_opts \
            $out_opts \
            {}_${m}${t} \
            \; 2>/dev/null
done
