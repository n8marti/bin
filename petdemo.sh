#!/bin/bash

# Setup PetDemo app in wine.

# Define variables.
petdemo_dir="${HOME}/petdemo"
export WINEPREFIX="${petdemo_dir}/.wine"
export WINEARCH=win32
export WINEDEBUG=fixme-all
app="$WINEPREFIX/drive_c/Program Files/JumpStart World/Pet Playground/JSWorldDemo.exe"
app_name=$(basename "$app")
app_dir=$(dirname "$app")
installer_dir="${petdemo_dir}/installer"
app_desktop="/desktop=PetDemo,800x600"

ensure_pkg_installation() {
    local pkg="$1"
    if [[ ! $(is_pkg_installed "$pkg") ]]; then
        echo "Installing $pkg..."
        if [[ "$pkg" == 'winehq-stable' ]]; then
            echo "  adding winehq repo..."
            sudo dpkg --add-architecture i386
            wget -nc https://dl.winehq.org/wine-builds/winehq.key
            sudo apt-key add winehq.key
            sudo add-apt-repository -y "deb https://dl.winehq.org/wine-builds/ubuntu/ $(lsb_release -sc) main"
        fi
        sudo apt-get -y update
        if [[ $? -ne 0 ]]; then
            echo "ERROR: Failed to update package lists."
            exit 1
        fi
        sudo apt-get -y install "$pkg"
        if [[ $? -ne 0 ]]; then
            echo "ERROR: Failed to install $pkg."
            exit 1
        fi
    fi
}

is_pkg_installed() {
    local pkg="$1"
    dpkg --list "$pkg" 2>/dev/null | grep "$pkg"
    return $?
}

# Ensure system driver config for wine compatibility.
mdapi_conf=/etc/sysctl.d/60-mdapi.conf
if [[ -d /sys/devices/i915 && ! -e "$mdapi_conf" ]]; then
    echo "dev.i915.perf_stream_paranoid = 0" | sudo tee "$mdapi_conf"
    sudo systemctl restart systemd-sysctl
fi

# Ensure wine installation.
ensure_pkg_installation "winehq-stable"
ensure_pkg_installation "winetricks"

# Ensure WINEPREFIX exists.
if [[ ! -d "$WINEPREFIX" ]]; then
    mkdir -p "$HOME/petdemo"
    wine wineboot
fi

# Parse commandline args.
if [[ -n "$@" ]]; then
    echo "Running command: $@"
    "$@"
    exit 0
fi

# Clear wine drivers key.
# wine reg delete "HKCU\\Software\\Wine\\Drivers" /f 2>&1 >/dev/null

# Set MIDI registry key.
wine reg add "HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Multimedia\\MIDIMap" \
    -v "CurrentInstrument" /t REG_SZ /d "#0" /f 2>&1 >/dev/null

# Ensure WMP10 is installed.
wmp_exe="$WINEPREFIX/drive_c/Program Files/Windows Media Player/wmlaunch.exe"
if [[ ! -x "$wmp_exe" ]]; then
    winetricks --force --unattended wmp10
fi

# Ensure proper Windows version.
wine winecfg -v winxp

# Ensure DirectX->Vulcan layer.
winetricks --unattended dxvk

# Ensure app is installed.
if [[ ! -x "$app" ]]; then
    if [[ ! -d "$installer_dir" ]]; then
        echo "ERROR: Installer folder not found: $installer_dir"
        exit 1
    fi
    cd "$installer_dir"
    wine explorer "$app_desktop" ./AUTORUN.EXE
    exit 0
fi

# Run app.
cd "$app_dir"
wine explorer "$app_desktop" "$app_name"
