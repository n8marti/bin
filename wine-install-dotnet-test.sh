#!/bin/bash

# Install dotnet in a clean 64-bit prefix to see if it succeeds.

dir="${HOME}/dotnet-test"

# Set default variables.
wt_cmd="winetricks"
mode='default'
v_log='40'
versions='40'

export WINEPREFIX="${dir}/wine-prefix"
export WINELOADER=$(which wine64)
export WINELOADER=$(which wine)
export WINEDLLOVERRIDES="mscoree,mshtml="

install_dotnet() {
    start=$(date +%s)
    mkdir -p "$dir" >/dev/null
    log="${dir}/dotnet${1}-${mode}-$(date +%Y-%m-%d-%H%M%S).log"
    echo "winetricks command: $wt_cmd" | tee -a "$log"
    rm -rf "$WINEPREFIX"

    for v in $2; do
        $wt_cmd "dotnet${v}" 2>&1 | tee -a "$log"
        verify_dotnet "$v" | tee -a "$log"
    done

    end=$(date +%s)
    dur=$((end-start))
    echo "Duration: ${dur}s" | tee -a "$log"
}

verify_dotnet() {
    status=$(winetricks list-installed | grep "dotnet${1}" 2>/dev/null)
    if [[ -n "$status" ]]; then
        echo "dotnet${1} installed"
    else
        echo "dotnet${1} NOT installed"
    fi
}

while getopts ':cdqv:' o; do
    case "$o" in
        c) # check .NET install
            verify=1
            ;;
        d) # debug
            set -x
            ;;
        q) # quiet mode
            wt_cmd="winetricks -q"
            mode=quiet
            ;;
        v) # .NET version
            v_log="$OPTARG"
            versions="$OPTARG"
            if [[ "$OPTARG" == '4048' ]]; then
                versions='40 48'
            fi
            ;;
        *)
            echo "Error: Unhandled option: $o"
            ;;
    esac
done
shift $((OPTIND-1))

# Enable check for existing dotnet installation.
if [[ -n "$verify" ]]; then
    for v in $versions; do
        verify_dotnet "$v"
    done
    exit 0
fi

# Install dotnet.
install_dotnet "$v_log" "$versions"
