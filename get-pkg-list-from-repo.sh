#!/bin/bash

# Get list of debian packages found at the given repository address.
#   Repo can be specified as an address or an apt source file.

# List repo secions:
#   curl -H 'User-Agent: Debian APT-HTTP/1.3' https://download.virtualbox.org/virtualbox/debian/dists/focal/Release
# List repo section packages:
#   curl -H 'User-Agent: Debian APT-HTTP/1.3' https://download.virtualbox.org/virtualbox/debian/dists/focal/contrib/binary-amd64/Packages
# Example repo source entry:
#   cat /etc/apt/sources.lists.d/virtualbox.list
#   deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian focal contrib

echo_err() {
    echo "$@" >&2
}

get_input_string_type() {
    local string="$1"
    local type='link'
    if [[ "${string:0:1}" == '/' ]]; then
        type='file'
    fi
    echo "$type"
}

verify_input() {
    local type="$1"
    local string="$2"
    if [[ "$type" == 'file' ]]; then
        verify_input_file "$2"
    elif [[ "$type" == 'link' ]]; then
        verify_input_link "$2"
    fi
}

verify_input_file() {
    local file="$1"
    if [[ ! -r "$file" ]]; then
        echo_err "ERROR: Can't read file: $file"
        exit 1
    fi
}

verify_input_link() {
    local link="$1"
}

get_source_line() {
    local file="$1"
    lines=$(grep -Ev -e '^#' -e '^\s*$' "$file")
    if [[ $(echo "$lines" | wc -l) -gt 1 ]]; then
        echo_err "ERROR: More than one repository source found:"
        echo_err "$lines"
        exit 1
    elif [[ -z "$lines" ]]; then
        echo_err "ERROR: No repository source info found."
        exit 1
    else
        echo "$lines"
    fi
}

get_link_from_source_line() {
    local line="$1"
    arch='amd64'
    link=$(echo "$line" | sed -E 's|^deb.* (https?://.*)$|\1|' | sed 's| |/dists/|' | sed 's| |/|')
    link="${link}/binary-${arch}/Packages"
    echo "$link"
}


# Handle CLI options.
while getopts ":dh" o; do
    case "$o" in
        d) # debug
            set -x
            ;;
        h) # help
            echo_err "No help for you! Read the script!"
            exit 1
            ;;
        *) # fallback
            echo_err "Illegal move!"
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

# Get user input.
if [[ -z "$1" ]]; then
    read -p "Please enter either repository address or file path: " input_string
    if [[ -z "$input_string" ]]; then
        exit 1
    fi
else
    input_string="$1"
fi

# Get target repo from user input.
input_type=$(get_input_string_type "$input_string")

# Verify input; exit with message if error.
verify_input "$input_type" "$input_string"

# Define link to get package list from.
source_link="$input_string"
if [[ "$input_type" == 'file' ]]; then
    source_line=$(get_source_line "$input_string")
    source_link=$(get_link_from_source_line "$source_line")
fi

# Get package list.
# TODO: Maybe the file "Packages" doesn't exist. Also try "Packages.gz..."
curl -H 'User-Agent: Debian APT-HTTP/1.3' "$source_link"
