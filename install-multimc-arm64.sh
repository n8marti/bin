#!/bin/bash

# References:
#  - https://nicholasbering.ca/raspberry-pi/2020/10/18/minecraft-64bit-rpi4/ # overview
#  - https://jjtech0130.github.io/meta-multimc/ # old meta info
#  - https://github.com/theofficialgman/meta-multimc # meta info
#  - https://github.com/OpenHantek/openhantek/issues/221 # mesa GL
#  - https://forums.raspberrypi.com/viewtopic.php?t=245035 # mesa GL

LOCAL_SHARE_DIR="${HOME}/.local/share"
mmc_dir="${HOME}/MultiMC"
mkdir -p "$mmc_dir"
mmc_install_dir="${LOCAL_SHARE_DIR}/multimc"
mmc_bin="${mmc_install_dir}/DevLauncher"
mmc_meta_url="https://raw.githubusercontent.com/theofficialgman/meta-multimc/master-clean/index.json"
mmc_desktop_file="${LOCAL_SHARE_DIR}/applications/multimc.desktop"

check_dep() {
	missing_pkgs="$2"
	dpkg --list | grep "ii  $1" >/dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		missing_pkgs+=" $1 "
	fi
}

install_deps() {
	sudo apt-get update
	if [[ $? -ne 0 ]]; then
		exit 1
	fi
	sudo apt-get --yes install "$1"
	if [[ $? -ne 0 ]]; then
		exit 1
	fi
}

build_mmc() {
	local build_dir="${1}/build"
	local install_dir="$3"
	local src_dir="${1}/src"
	local meta_url="$2"
	cd "$1"
	git clone --recursive https://github.com/MultiMC/MultiMC5.git "$src_dir"
	# Maybe remnants af an old build are still there?
	rm -rf "${build_dir}/"*
	rm -rf "${install_dir}/bin"
	# Ensure necessary folders exist.
	mkdir -p "$build_dir" "$install_dir"
	cd "$build_dir"
	cmake \
	 	-DCMAKE_INSTALL_PREFIX="$install_dir" \
	 	-DLauncher_META_URL:STRING="$meta_url" \
	 	"$src_dir"
	make -j$(nproc) "$install_dir"
	cp "${src_dir}/launcher/package/ubuntu/multimc/opt/multimc/icon.svg" "$install_dir"
	cd ~
}

create_desktop_file() {
	# Set mesa overrides in env. The version numbers seem to be interdependent.
	> "$1"
	echo "[Desktop Entry]" >> "$1"
	echo "Categories=Game;" >> "$1"
	echo "Exec=env MESA_GL_VERSION_OVERRIDE=3.3 MESA_GLES_VERSION_OVERRIDE=3.1 MESA_GLSL_VERSION_OVERRIDE=300 $mmc_bin" >> "$1"
	echo "Icon=${mmc_install_dir}/icon.svg" >> "$1"
	echo "Keywords=game;Minecraft;" >> "$1"
	echo "Name=MultiMC 5" >> "$1"
	echo "Type=Application" >> "$1"
	echo "X-KDE-SubstituteUID=false" >> "$1"
}


# Ensure system dependencies are met and up-to-date.
missing_pkgs=
pkgs=" build-essential cmake libqt5core5a libqt5network5 libqt5gui5 qtbase5-dev zlib1g-dev libgl1-mesa-dev openjdk-8-jdk git "
for pkg in $pkgs; do
	check_dep "$pkg" "$missing_pkgs"
done
if [[ -n "$missing_pkgs" ]]; then
	install_deps "$missing_pkgs"
fi

# Ensure that DevLauncher exists and that binaries are built.
if [[ ! -x "$mmc_bin" ]]; then
	build_mmc "$mmc_dir" "$mmc_meta_url" "$mmc_install_dir"
fi

# Ensure desktop file exists.
if [[ ! -e "$mmc_desktop_file" ]]; then
	create_desktop_file "$mmc_desktop_file"
fi
