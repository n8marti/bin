#!/bin/bash

# Add a static IP/MAC entry for pfSense firewall.
# Handle 3 arguments:
#   1. MAC address
#   2. IP address
#   3. config file path

# Define global variables.
LAN_IF='opt4'

# Define functions.
add() {
    local mac="$1"
    local cid="$2"
    local ip="$3"
    local file="$4"
    xmlstarlet edit --inplace \
        --insert "//pfsense/dhcpd/$LAN_IF/failover_peerip" --type elem --name "staticmap" \
        --var parent '$prev' \
        --subnode '$parent' --type elem -name "mac" --value "$mac" \
        --subnode '$parent' --type elem -name "cid" --value "$cid" \
        --subnode '$parent' --type elem -name "ipaddr" --value "$ip" \
        --subnode '$parent' --type elem -name "hostname" --value "" \
        --subnode '$parent' --type elem -name "descr" --value "" \
        --subnode '$parent' --type elem -name "arp_table_static_entry" --value "" \
        --subnode '$parent' --type elem -name "filename" --value "" \
        --subnode '$parent' --type elem -name "rootpath" --value "" \
        --subnode '$parent' --type elem -name "defaultleasetime" --value "" \
        --subnode '$parent' --type elem -name "maxleasetime" --value "" \
        --subnode '$parent' --type elem -name "gateway" --value "" \
        --subnode '$parent' --type elem -name "domain" --value "" \
        --subnode '$parent' --type elem -name "domainsearchlist" --value "" \
        --subnode '$parent' --type elem -name "ddnsdomain" --value "" \
        --subnode '$parent' --type elem -name "ddnsdomainprimary" --value "" \
        --subnode '$parent' --type elem -name "ddnsdomainsecondary" --value "" \
        --subnode '$parent' --type elem -name "ddnsdomainkeyname" --value "" \
        --subnode '$parent' --type elem -name "ddnsdomainkeyalgorithm" --value "hmac-md5" \
        --subnode '$parent' --type elem -name "ddnsdomainkey" --value "" \
        --subnode '$parent' --type elem -name "tftp" --value "" \
        --subnode '$parent' --type elem -name "ldap" --value "" \
        --subnode '$parent' --type elem -name "nextserver" --value "" \
        --subnode '$parent' --type elem -name "filename32" --value "" \
        --subnode '$parent' --type elem -name "filename64" --value "" \
        --subnode '$parent' --type elem -name "filename32arm" --value "" \
        --subnode '$parent' --type elem -name "filename64arm" --value "" \
        --subnode '$parent' --type elem -name "numberoptions" --value "" \
        "$file"
}

search() {
    # argument: "ip=1.1.1.1,mac=00:00:00:00:00:00"

    # Split argument into terms.
    local t1="$(echo "$1" | awk -F',' '{print $1}')"
    local t2="$(echo "$1" | awk -F',' '{print $2}')"
    # Get keys and values from terms.
    local k1="$(echo "$t1" | awk -F'=' '{print $1}')"
    local v1="$(echo "$t1" | awk -F'=' '{print $2}')"
    local k2="$(echo "$t2" | awk -F'=' '{print $1}')"
    local v2="$(echo "$t2" | awk -F'=' '{print $2}')"

    # Set ip and mac variables.
    if [[ "$k1" == 'mac' ]]; then
        local mac="$v1"
        local ip="$v2"
    elif [[ "$k1" == 'ip' ]]; then
        local mac="$v2"
        local ip="$v1"
    else
        echo "ERROR: Bad search term: $1"
        exit 1
    fi

    # Search CONFIG for mac and ip.
    matched_ip=
    if [[ -n "$mac" ]]; then
        matched_ip=$(grep -A2 "<mac>$mac</mac>" "$2" | tail -n1)
        matched_ip="${matched_ip#*>}"
        matched_ip="${matched_ip%%<*}"
    fi
    matched_mac=
    if [[ -n "$ip" ]]; then
        matched_mac=$(grep -B2 "<ipaddr>$ip</ipaddr>" "$2" | head -n1)
        matched_mac="${matched_mac#*>}"
        matched_mac="${matched_mac%%<*}"
    fi

    # Output results.
    if [[ -n "$matched_mac" && -n "$matched_ip" ]]; then
        if [[ "$matched_mac" == "$mac" ]]; then
            # Entry already exists.
            echo "${matched_mac}=${ip}"
        else
            # Separate entires exist for both mac and ip.
            echo "${matched_mac}=${ip}, ${mac}=${matched_ip}"
        fi
    elif [[ -n "$matched_mac" ]]; then
        # Entry already exists for given ip.
        echo "${matched_mac}=${ip}"
        # Entry already exists for given mac.
    elif [[ -n "$matched_ip" ]]; then
        echo "${mac}=${matched_ip}"
    fi
}

show() {
    xmlstarlet select --template --copy-of "//pfsense/dhcpd/$LAN_IF/staticmap" -n "$1"
}

# Parse arguments.
ACTION=
if [[ -n "$4" ]]; then
    # Add entry to config file.
    CONFIG="$4"
    IP="$3"
    CID="$2"
    MAC="$1"
    ACTION='add'
elif [[ -n "$3" ]]; then
    ACTION='help'
elif [[ -n "$2" ]]; then
    # Check if entry is in config file.
    CONFIG="$2"
    TERM="$1"
    ACTION='search'
else
    if [[ "$1" == '-h' || "$1" == '--help' || -z "$1" ]]; then
        ACTION='help'
    else
        CONFIG="$1"
        ACTION='show'
    fi
fi

# Hande use cases.
usage="USAGE: $0 MAC_ADDRESS [DEVICE_LABEL IP_ADDRESS] CONFIG.xml"
case $ACTION in
    'add')
        match=$(search "mac=${MAC},ip=${IP}" "$CONFIG")
        if [[ -z "$match" ]]; then
            add "$MAC" "$CID" "$IP" "$CONFIG"
        else
            echo "INFO: Entry exists: $match"
        fi
        # Fix empty tag formatting.
        sed -i -r 's|<(.*)/>$|<\1></\1>|' "$CONFIG"
        # Convert double-spaces to tabs.
        sed -i 's|  |\t|g' "$CONFIG"
        exit 0
        ;;
    'help')
        echo "$usage"
        exit 0
        ;;
    'search')
        search "$TERM" "$CONFIG"
        exit 0
        ;;
    'show')
        show "$CONFIG"
        exit 0
        ;;
    *)
        echo "$usage"
        exit 1
        ;;
esac
