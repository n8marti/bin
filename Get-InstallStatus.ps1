Function Get-InstallStatus {
    param($AppName)

    $RPath = @(
        'HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*'
        'HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
    )
    $result = Get-ItemProperty $RPath `
        | .{process{if($_.DisplayName -and $_.UninstallString) { $_ } }} `
        | Select-Object DisplayName, Publisher, InstallDate, DisplayVersion, UninstallString `
        | Sort DisplayName `
        | Where-Object {$_.DisplayName -like "*$AppName*"}

    If ($null -eq $result) {
        Write-Host "$AppName not installed."
        return 0
    #    (Start-Process -FilePath $msiFile -ArgumentList $msiArgs -Wait -Passthru).ExitCode
    } Else {
        $name = $result.DisplayName
        $version = $result.DisplayVersion
        Write-Host "$name already installed."
        return 1
    }
}