#!/bin/bash

if [[ $1 ]]; then
    start=$1
else
    start=10000
fi

if [[ $2 ]]; then
    end=$2
else
    end=10100
fi

server=216.58.223.206 # google.com
server=89.185.43.6 # rfi.fr

echo "Testing ports $start to $end"

gnome-terminal -- ping rfi.fr

for port in $(seq $start $end); do
    (netcat -p $port $server 443 >/dev/null 2>&1 & )
done

sessions=$(netstat -npt 2>/dev/null | grep ESTABLISHED | wc -l)
echo "$sessions tcp sessions (established)"

ping -c10 rfi.fr >/dev/null 2>&1
ret=$?

for p in $(pidof netcat); do
    kill $p 2>/dev/null
done

if [[ ! $ret == '0' ]]; then
    echo "failed! [$ret]"
fi
