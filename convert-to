#!/usr/bin/env python3

import sys


HEX_TABLE = {
    'A': 10,
    'B': 11,
    'C': 12,
    'D': 13,
    'E': 14,
    'F': 15,
}


def exit_unsupported_base(base):
    print(f"Base \"{base}\" not supported.")
    exit(1)

def are_same(value_in, value_fixed):
    # Only compare as much of the value as is given.
    return value_in[:len(value_in)] == value_fixed[:len(value_in)]

def convert_decimal_to(base, decimal_value):
    decimal_value = int(decimal_value)
    converted_value = []
    i = 0
    while decimal_value >= base**i:
        i += 1
    i -= 1
    remainder = decimal_value
    while i >= 0:
        value = remainder // base**i
        if base == 16:
            for k, v in HEX_TABLE.items():
                if value == v:
                    value = k
        converted_value.append(str(value))
        remainder = remainder % base**i
        i -= 1
    return ''.join(converted_value)

def convert_to_decimal(base, base_value):
    base_value = base_value.replace('-', '')
    base_value = [d for d in base_value]
    decimal_value = 0
    i = 0
    while base_value:
        value = base_value.pop()
        if base == 16:
            value = HEX_TABLE.get(value.upper(), value)
        value = int(value) * base**i
        decimal_value += value
        i += 1
    return decimal_value


if sys.argv[1] in ['-h', '--help']:
    print(f"usage: {sys.argv[0]} <end_base> <start_base> <value>")
    print("bases: b[inary], d[ecimal], h[exadecimal]")
    exit()

base_out = sys.argv[1]
base_in = sys.argv[2]
value_in = sys.argv[3]
if len(value_in) > 2 and value_in[1].lower() == 'x':
    value_in = value_in[2:]

if are_same(base_in, "decimal"):
    if are_same(base_out, "binary"):
        binary_value = convert_decimal_to(2, value_in)
        print(binary_value)
    elif are_same(base_out, "hexidecimal"):
        hex_value = convert_decimal_to(16, value_in)
        print(hex_value)
    else:
        exit_unsupported_base(base_out)
elif are_same(base_in, "binary"):
    if are_same(base_out, "decimal"):
        decimal_value = convert_to_decimal(2, value_in)
        print(decimal_value)
    elif are_same(base_out, "hexidecimal"):
        decimal_value = convert_to_decimal(2, value_in)
        hex_value = convert_decimal_to(16, decimal_value)
        print(hex_value)
elif are_same(base_in, "hexadecimal"):
    if are_same(base_out, "decimal"):
        decimal_value = convert_to_decimal(16, value_in)
        print(decimal_value)
    elif are_same(base_out, "binary"):
        decimal_value = convert_to_decimal(16, value_in)
        binary_value = convert_decimal_to(2, decimal_value)
        print(binary_value)
    else:
        exit_unsupported_base(base_in)
else:
    exit_unsupported_base(base_in)
