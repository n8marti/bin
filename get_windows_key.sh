#!/bin/bash
# written by Jeff Sadowski
# credit
###################################################
# Pavel Hruška, Scott Skahht, and Philip M for writting
# https://github.com/mrpeardotnet/WinProdKeyFinder/blob/master/WinProdKeyFind/KeyDecoder.cs
# that I got my conversion code from
#
# I used the comments on the sudo code from
# https://askubuntu.com/questions/953126/can-i-recover-my-windows-product-key-from-ubuntu
# by MrPaulch
#
# and the creator of chntpw
#
# Petter Nordahl-Hagen
# without which I would not be able to get the key in linux
#
# also the creators of ntfs-3g, linux and bash

parted -l 2>/dev/null |grep -e ntfs -e fat -e Disk|grep -v Flags
#get the first mac address that isn't a loopback address
# loopback will have all zeros
MAC=$(cat /sys/class/net/*/address|grep -v 00:00:00:00:00:00|head -n 1|sed "s/:/-/g")
if [ "$1" = "" ];then
 echo "mount the Windows share then give this script the path where you mounted it"
 exit
fi
cd $1
#
# This way will work no matter what the capitalization is
next=$(find ./ -maxdepth 1 -iname windows);cd ${next}
next=$(find ./ -maxdepth 1 -iname system32);cd ${next}
next=$(find ./ -maxdepth 1 -iname config);cd ${next}
file=$(find ./ -maxdepth 1 -iname software)
#echo $(pwd)${file:1}
#Get the necissary keys
#get the version key
VERSION=$((16#$(echo -e "cat \\Microsoft\\Windows NT\\CurrentVersion\\CurrentMajorVersionNumber\nq\n" | chntpw -e ${file}|grep "^0x"|cut -dx -f2)))
hexPid_csv_full=$(echo $(echo -e "hex \\Microsoft\\Windows NT\\CurrentVersion\\DigitalProductId\nq\n" | chntpw -e ${file}|grep "^:"|cut -b 9-55)|sed 's/ /,/g' | tr '[:upper:]' '[:lower:]')
# get the subset 53 to 68 of the registry entry
hexPid_csv=$(echo $(echo -e "hex \\Microsoft\\Windows NT\\CurrentVersion\\DigitalProductId\nq\n" | chntpw -e ${file}|grep "^:"|cut -b 9-55)|sed 's/ /,/g' | tr '[:upper:]' '[:lower:]'|cut -d, -f 53-68)
echo "${hexPid_csv_full}" > /custom/DigitalProductId_${MAC}.txt
#formatted output
spread()
{
 key=$1
 echo ${key:0:5}-${key:5:5}-${key:10:5}-${key:15:5}-${key:20:5}
}
# almost a direct conversion of c# code from
# https://github.com/mrpeardotnet/WinProdKeyFinder/blob/master/WinProdKeyFind/KeyDecoder.cs
# however most of this looks similar to sudo code I found
# https://askubuntu.com/questions/953126/can-i-recover-my-windows-product-key-from-ubuntu
DecodeProductKey()
{
digits=(B C D F G H J K M P Q R T V W X Y 2 3 4 6 7 8 9)
for j in {0..15};do
#Populate the Pid array from the values found in the registry
 Pid[$j]=$((16#$(echo ${hexPid_csv}|cut -d, -f $(($j+1)))))
done
if [ "$1" = "8+" ];then
# modifications needed for getting the windows 8+ key
 isWin8=$(($((${Pid[14]}/6))&1))
 Pid[14]=$(( $(( ${Pid[14]}&247 )) | $(( $(( ${isWin8} & 2 )) * 4 )) ))
fi
key=""
last=0
for i in {24..0};do
 current=0
 for j in {14..0};do
# Shift the current contents of c to the left by 1 byte 
# and add it with the next byte of our id
  current=$((${current}*256))
  current=$((${Pid[$j]} + current))
# Put the result of the divison back into the array
  Pid[$j]=$((${current}/24))
# Calculate remainder of c
  current=$((${current}%24))
  last=${current}
 done
# Take character at position c and prepend it to the ProductKey
 key="${digits[${current}]}${key}"
done
if [ "$1" = "8+" ];then
# another modification needed for a windows 8+ key
 key="${key:1:${last}}N${key:$((${last}+1)):24}"
 echo -n "Windows 8+ key: "
 else
 echo -n "Windows 7- key: "
fi
spread "${key}"
}
if [ "$VERSION" -gt "7" ];then
 DecodeProductKey 8+
else
 DecodeProductKey
fi
