#!/bin/bash

echo "This is untested!"
exit 1

# Creating a bootable ISO.
# Windows 10 - use -udf.  (!! WinXP doesn't work with udf enabled !!)
#extract CD boot catalog with "geteltorito <name>.iso

mkisofs -v -o ~/iso/wX.iso  -udf -D -N -J     -eltorito-boot isolinux/isolinux.bin -eltorito-catalog isolinux/boot.cat  -no-emul-boot -boot-load-size 4 -boot-info-table -hide isolinux/boot.cat -hide-joliet isolinux/boot.cat -hidden isolinux/boot.cat  ..
