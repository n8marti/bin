#!/bin/bash

# Unencrypt, properly mount, and then properly unmount a BitLocker drive.

clean_exit() {
    prc=$1
    sudo umount "${mnt_pt}/drive"
    sudo umount "${mnt_pt}/file"
    rc=$?
    sudo rmdir "${mnt_pt}"/{file,drive}
    sudo rmdir "$mnt_pt"
    ec=$((prc + rc))
    exit $ec
}

# Check for app.
if [[ ! $(which dislocker) ]]; then
    echo "Need to install dislocker:"
    echo "sudo apt install dislocker"
fi

# Get action.
if [[ $1 ]]; then
    if [[ $1 == '-h' ]] || [[ $1 == '--help' ]]; then
        echo "Usage: $0 [-m|-u]" "/dev/sd[a-z][1-9]"
        exit 0
    else
        action="$1"
    fi
else
    read -p "Mount (-m) or unmount (-u)?: " action
fi

# Define mount points.
mnt_pt="/mnt/bitlocker"

# Unmount, if chosen.
if [[ $action == '-u' ]]; then
    clean_exit 0
fi

# Get drive.
if [[ $2 ]]; then
    bl_drive="$2"
    if [[ ! -e "$bl_drive" ]]; then
        echo "$bl_drive doesn't exist."
        exit 1
    fi
else
    read -p "Give device location (e.g. /dev/sda1): " bl_drive
fi

# Ensure subfolders for mounting.
sudo mkdir -p "${mnt_pt}"/{file,drive}

# Get dislocker mount options.
read -p "dislocker options? (e.g. \"-u\" for password, \"-r\" for read-only): " d_opts
read -p "mount options? (e.g. \"-r\" for read-only): " m_opts

if [[ $action == '-m' ]]; then
    if [[ -d "${mnt_pt}/file" ]] && [[ -d "${mnt_pt}/drive" ]]; then
        sudo dislocker $d_opts -V "$bl_drive" "${mnt_pt}/file"
        if [[ $? -ne 0 ]]; then
            echo "File mount failed."
            clean_exit 1
        fi
        sudo mount $m_opts -o loop "${mnt_pt}/file/dislocker-file" "${mnt_pt}/drive"
        if [[ $? -ne 0 ]]; then
            echo "Drive mount failed. Cleaning up."
            clean_exit 1
        fi
        xdg-open "${mnt_pt}/drive"
    else
        echo "Folder doesn't exist: ${mnt_pt}"/{file,drive}
        clean_exit 1
    fi
fi
