#!/bin/bash

usage() {
    echo "$0 SS.S|FR /video/file.vid"
}

help() {
    usage
    echo
    echo "Export as JPG video frame specified by frame number or time in decimal seconds."
}

infile="$2"
ref="$1"
type="frame"
if [[ "$ref" == *"."* ]]; then
    type="time"
fi

if [[ "$type" == 'time' ]]; then
    name="${infile%.*}_t${ref}.jpg"
    ffmpeg -y -ss "$ref" -i "$infile" -frames:v 1 "$name" 2>/dev/null
    ec=$?
else
    name="${infile%.*}_f${ref}.jpg"
    ffmpeg -y -i "$infile" -vf "select=eq(n\,$((ref - 1)))" -frames:v 1 "$name" 2>/dev/null
    ec=$?
fi

if [[ $ec -eq 0 ]]; then
    echo "${PWD}/${name}"
fi
