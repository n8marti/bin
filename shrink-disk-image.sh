#!/bin/bash

# Minimize the size of a USB disk image for faster reproduction.
# 1. Make USB disk image.
# 2. Shrink image partition.
# 3. Shrink image filesystem.
# 4. Shrink image file.
# REF: https://askubuntu.com/questions/1174487/re-size-the-img-for-smaller-sd-card-how-to-shrink-a-bootable-sd-card-image

LANG=C
DISK=''
IMAGE_PATH=''


show_usage() {
    echo "Usage: $0 [-h] [-d] [DISK_IMAGE.img | /dev/DEVICE]"
}

show_help() {
    show_usage
    echo "
    Shrink given DISK_IMAGE to as small as possible. If /dev/DEVICE is given,
    make an image of the device, then shrink it down.
    NOTE: This script requires that the device have only 1 partition."
}

parse_args() {
    while getopts "dh" o; do
        case "${o}" in
            d) # debug
                set -x
                ;;
            h) # help
                show_help
                exit 0
                ;;
            *) # other
                show_help
                exit 1
                ;;
        esac
    done
    shift $((OPTIND-1))

    if [[ $1 ]]; then
        if [[ "${1##*.}" == 'img' ]]; then
            full_path=$(realpath "$1")
            verify_image_path "$full_path"
            IMAGE_PATH="$full_path"
        elif [[ "${1%/*}" == '/dev' ]]; then
            verify_disk "$1"
            DISK="$1"
        else
            show_help
        fi
    fi
}

verify_disk() {
    disk="$1"
    # An entry is valid only if it's found as a hotpluggable disk.
    valid=$(lsblk -npo PATH,TYPE,HOTPLUG | grep 'disk' | grep ' 1' | grep "$disk")
    if [[ ! "$valid" ]]; then
        echo "Error: Not a valid disk: $disk"
        exit 9
    fi
}

verify_one_partition() {
    disk="$1"
    partitions=$(lsblk -npo PATH,TYPE "$disk" | grep 'part')
    if [[ $(echo "$partitions" | wc -l) -gt 1 ]]; then
        echo "Error: More than 1 partition on disk. Aborting."
        exit 2
    fi
}

verify_image_path() {
    image_path="$1"
    if [[ ! -f "$image_path" ]]; then
        echo "Error: Invalid image path: $image_path"
        exit 1
    fi
}

verify_fat_filesystem() {
    fstype="$1"

    if [[ $fstype != 'fat16' ]] && [[ $fstype != 'fat32' ]]; then
        echo "Error: Not FAT filesystem: $fstype"
        exit 2
    fi
}

verify_target_disk_space() {
    bytes_needed="$1"
    type="$2"
    if [[ "$type" == 'device' ]]; then
        bytes_needed=$((bytes_needed * 2))
    fi
    file_system=$(df "$PWD" | grep -v Filesystem | awk '{print $1}')
    bytes_available=$(df "$PWD" --block-size=1 | grep -v Filesystem | awk '{print $4}')
    if [[ "$bytes_available" -le "$bytes_needed" ]]; then
        echo "Error: Not enough space on ${file_system}."
        exit 3
    fi
}

get_disk() {
    # Show a list of available disks.
    lsblk -npo PATH,SERIAL,HOTPLUG,SIZE,TYPE | grep 'disk' | grep ' 1'
    echo
    read -p "Which disk do you want to image? [ex. /dev/sdb]: " ans
    DISK="$ans"
}

unmount_disk_partitions() {
    disk="$1"

    # Unmount partition.
    partition=$(lsblk -npo PATH,TYPE "$disk" | grep 'part' | awk '{print $1}')
    if [[ -z "$partition" ]]; then
        echo "ERROR: No partition found."
        exit 1
    fi
    sudo umount $partition
    if [[ $? -eq 32 ]]; then
        # Disk already unmounted.
        return
    elif [[ $? -ne 0 ]]; then
        echo "ERROR: $partition not properly unmounted."
        exit 1
    fi
    echo "Successfully unmounted ${partition}."
}

ensure_disk_image() {
    disk="$1"

    # Make disk label from device serial number and size.
    LABEL=$(lsblk -npo PATH,SERIAL,SIZE,TYPE "$disk" | grep 'disk' | tr -s ' ' | cut -d' ' -f2,3 | tr ' ' '-')
    IMAGE_PATH="${HOME}/${LABEL}.img"
    # Check if image already exists.
    ans='overwrite'
    if [[ -f "$IMAGE_PATH" ]]; then
        read -p "$IMAGE_PATH exists: (k)eep or (o)verwrite? [k]: " ans
    fi
    # Ensure disk image.
    if [[ "$ans" == 'overwrite' ]] || [[ "$ans" == 'O' ]] || [[ "$ans" == 'o' ]]; then
        echo -e "\nCreating image at $IMAGE_PATH"
        rm -f "$IMAGE_PATH"
        sudo dd if="$disk" of="$IMAGE_PATH" bs=4M status=progress
    fi
    sudo chown $USER:$USER "$IMAGE_PATH"
}

get_disk_info() {
    local image_path="$1"
    local info=$(sudo parted -ms "$image_path" unit B print)
    # Relevant info starts on line after 'BYT;'.
    local relevant_info=$(echo "$info" | grep -A3 'BYT;' | tail -n +2)
    local disk_info=$(echo "$relevant_info" | head -n 1)
    # local disk_info=$(echo "$info" | tail -n +2 | head -n 1)
    # Partition info is 2 lines after 'BYT;' and following.
    # local part_info=$(echo "$info" | tail -n +3)
    local part_info=$(echo "$relevant_info" | tail -n +2)
    if [[ $(echo "$part_info" | wc -l) -gt 1 ]]; then
        echo "Error: More than 1 partition on disk. Aborting."
        exit 2
    fi

    # Define disk variables.
    DISK_NAME=$(echo "$disk_info" | awk -F':' '{print $1}')
    local disk_size_B=$(echo "$disk_info" | awk -F':' '{print $2}')
    DISK_SIZE=${disk_size_B%B}
    SECTOR_SIZE=$(echo "$disk_info" | awk -F':' '{print $4}')
    # Define partition variables.
    PART_NO=$(echo "$part_info" | awk -F':' '{print $1}')
    local startB_B=$(echo "$part_info" | awk -F':' '{print $2}')
    STARTB=${startB_B%B}
    local endB_B=$(echo "$part_info" | awk -F':' '{print $3}')
    ENDB=${endB_B%B}
    local sizeB_B=$(echo "$part_info" | awk -F':' '{print $4}')
    SIZEB=${sizeB_B%B}
    FSTYPE=$(echo "$part_info" | awk -F':' '{print $5}')
    FLAGS=$(echo "$part_info" | awk -F':' '{print $6}')
    # echo -e "DISK_NAME:${DISK_NAME}\nDISK_SIZE:${DISK_SIZE}\nSECTOR_SIZE:${SECTOR_SIZE}\nPART_NO:${PART_NO}\nSTARTB:${STARTB}\nENDB:${ENDB}\nSIZEB:${SIZEB}\nFSTYPE:${FSTYPE}\nFLAGS:${FLAGS}"
}

get_loop_device() {
    # Get next free loop device.
    LOOP_DEV=$(sudo losetup -f)
}

attach_image() {
    image_path="$1"
    loop_dev="$2"

    verify_image_path "$image_path"
    # Attach image to device.
    sudo losetup "$loop_dev" "$image_path"
    if [[ $? -eq 0 ]]; then
        echo "Successfully attached $image_path to $loop_dev"
    else
        echo "Failed to attach $image_path to $loop_dev"
        exit 1
    fi
    # Discover device partitions.
    sudo partprobe "$loop_dev"
    # Get partition path.
    PARTITION_PATH=$(lsblk -po PATH,LABEL | grep "$loop_dev" | grep -vw "$loop_dev" | awk '{print $1}')
}

mount_image_partition() {
    partition_path="$1"

    MOUNT_POINT=/mnt/temp_usb
    sudo mkdir -p "$MOUNT_POINT"
    sudo mount "$partition_path" "$MOUNT_POINT"
    if [[ $? -ne 0 ]]; then
        echo "Error: Failed to mount $partition_path to $MOUNT_POINT"
        unmount_image "$MOUNT_POINT"
        exit 1
    fi
}

unmount_image() {
    mount_point="$1"
    sudo umount "$mount_point"
    sudo rm -d "$mount_point"
}

detach_image() {
    loop_dev="$1"
    sudo losetup -d "$loop_dev"
}

make_image_backup() {
    MIN_IMAGE_PATH="${1%.*}-min.img"
    # Backup original image file.
    echo -e "\nCopying image to $MIN_IMAGE_PATH"
    rm -f "$MIN_IMAGE_PATH"
    rsync -P "$1" "$MIN_IMAGE_PATH"
}

get_label() {
    default="$1"
    LABEL="$default"
    echo
    read -p "Enter your desired partition label [$default]: " ans
    if [[ "$ans" ]]; then
        LABEL="$ans"
    fi
}

run_fsck() {
    echo -e "\nChecking FAT filesystem at $1..."
    sudo fsck.fat -a -w "$1"
}

get_min_size_bytes() {
    sudo fatresize --info "$1" | grep 'Min size:' | awk '{print $3}'
}

get_final_size_bytes() {
    local new_size_bytes="$2"
    local min_bytes=$(get_min_size_bytes "$1")
    if [[ "$new_size_bytes" -lt "$min_bytes" ]]; then
        new_size_bytes="$min_bytes"
    fi
    echo "$min_bytes"
}

shrink_partition() {
    echo -e "\nShrinking partition $1 to $2 B..."
    # sudo parted "$PARTITION_PATH" unit B print
    sudo parted -s "$1" resizepart 1 "${2}B"
}

shrink_filesystem() {
    local final_size_bytes=$(get_final_size_bytes "$1" "$2")
    echo -e "\nShrinking filesystem at $1 to $final_size_bytes B..."
    sudo fatresize -v --progress --size "$final_size_bytes" "$1"
}

### Handle arguments.
parse_args "$@"

### 1. Ensure disk image.
if [[ ! -f $IMAGE_PATH ]]; then
    # Get disk from user if not passed as arg.
    if [[ -z "$DISK" ]]; then
        get_disk
    fi
    # Verify disk properties.
    get_disk_info "$DISK"
    verify_target_disk_space "$DISK_SIZE" "device"
    verify_disk "$DISK"
    verify_one_partition "$DISK"
    verify_fat_filesystem "$FSTYPE"
    # Make disk image.
    unmount_disk_partitions "$DISK"
    ensure_disk_image "$DISK"
fi


### 2. Shrink image partition.
MIN_IMAGE_PATH=''
verify_target_disk_space "$DISK_SIZE"
make_image_backup "$IMAGE_PATH"
get_disk_info "$MIN_IMAGE_PATH"
verify_fat_filesystem "$FSTYPE"

# Mount disk image as loop device.
LOOP_DEV=''
get_loop_device
PARTITION_PATH=''
MOUNT_POINT=''
attach_image "$MIN_IMAGE_PATH" "$LOOP_DEV"
# Update filesystem label.
LABEL=''
get_label "$FSTYPE"
echo "Labeling filesystem at $PARTITION_PATH as $LABEL..."
sudo fatlabel "$PARTITION_PATH" "$LABEL"
# Mount partition.
mount_image_partition "$PARTITION_PATH"

# Get volume usage of current partition.
bytes_used=$(df "$MOUNT_POINT" --block-size=1 | grep -v Filesystem | awk '{print $3}')
echo "Number of bytes used on disk image: $bytes_used B"
new_size_bytes=$((bytes_used + 1))
unmount_image "$MOUNT_POINT"
# Fix any existing disk errors.
run_fsck "$PARTITION_PATH"
# Reduce partition size.
final_size_bytes=$(get_final_size_bytes "$PARTITION_PATH" "$new_size_bytes")
shrink_partition "$PARTITION_PATH" "$final_size_bytes"

### 3. Shrink image filesystem.
shrink_filesystem "$PARTITION_PATH" "$final_size_bytes"
ec=$?
# Remove loop device.
detach_image "$LOOP_DEV"
if [[ $ec -ne 0 ]]; then
    # Fatresize failed. Remove output image and exit.
    echo "Error: Failed to resize FAT partition. Filesystem not shrunken."
    rm "$MIN_IMAGE_PATH"
    exit 1
fi
echo


### 4. Shrink image file.
echo -e "\nShrinking disk image file at $MIN_IMAGE_PATH to $new_size_bytes B..."
truncate --size=$((final_size_bytes + 1)) "$MIN_IMAGE_PATH"
# Rename file to match given LABEL.
dest="${MIN_IMAGE_PATH%/*}/${LABEL}.img"
mv "$MIN_IMAGE_PATH" "$dest"
echo "Disk image ready at $dest"
