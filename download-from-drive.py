#!/usr/bin/env python3

# FIXME: This only works for files that have the "too big to scan" warning page.
# Some guidance here: https://webapps.stackexchange.com/a/59427
# Examples:
# - MP4: https://drive.google.com/file/d/1C8aNo8NVwrUUJ1y9lSazZstdCIC8K8jx/view?usp=sharing
# - DOCX: https://docs.google.com/document/d/1qd2AgubUvT3vJSDAdD8eDeZTYmeMFqrF/edit?usp=drive_link&ouid=100657666845524549385&rtpof=true&sd=true
# - JPG: https://drive.google.com/file/d/0B93x4vEJEyYDU3lrVElKU2MtSlU/view?usp=sharing&resourcekey=0-wkIMfWeNewCLDGCHzXh7tg

import re
import subprocess
import sys
from pathlib import Path

file_str = sys.argv[1]
if len(file_str) < 40: # fileId size = 33 on 2023-11
    file_id = file_str
else:
    file_id = file_str.split('/')[5]

file_url = f'https://drive.google.com/uc?export=download&id={file_id}'

outdir = Path('.').resolve().expanduser()
outfile = outdir / 'download-anyway.html'

def start_download(url, outfile=None):
    if outfile is None:
        print(f"Error: No outfile given.")
        exit(1)

    wget_cmd = [
        'wget',
        '--no-check-certificate',
        '--show-progress',
        '--progress=dot:giga',
        url,
        '-O', outfile,
    ]
    with subprocess.Popen(
        wget_cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
        ) as p:
        for line in iter(p.stdout.readline, b''):
            print(line.decode().rstrip())
        p.wait()
        if p.returncode != 0:
            # print(r.stdout.decode())
            print(p.stderr.decode())

if __name__ == '__main__':
    start_download(file_url, outfile=outfile)
    if outfile.read_bytes()[:15].decode().lower() == '<!doctype html>':
        fdata = outfile.read_text()

        url_list = re.findall(r'action="(https://drive.google.com/uc\S+)"', fdata)
        if len(url_list) == 0:
            followup_list = re.findall(r'followup=(https://drive.google.com/uc[^\s&]+)', fdata)
            if len(followup_list) == 0:
                print(f"Error: No updated URL found.")
                Path(outdir / 'download-anyway.html').unlink(missing_ok=True)
                exit(1)
            file_url = followup_list[-1]
        else:
            file_url = url_list[-1]
        file_url = re.sub('%26', '&', file_url)
        file_url = re.sub('%3D', '=', file_url)
        file_url = re.sub('&amp;', '&', file_url)

        filenames = re.findall(r'<a href="\S+">(.*)</a>', fdata)
        if len(filenames) == 0:
            print(f"Error: No output filename found. Unable to download.")
            Path(outdir / 'download-anyway.html').unlink(missing_ok=True)
            exit(1)
        outfile = outdir / filenames[-1]

        start_download(file_url, outfile=outfile)

    Path(outdir / 'download-anyway.html').unlink(missing_ok=True)
