#!/bin/bash

# Run a mounted disk as a VirtualBox virtual machine
script="$0"

# Script must be run with sudo.
if [ $(id -u) -ne 0 ]; then
    # OUT will be <blank> if user not in sudo group
    OUT=$(groups $(whoami) | grep "sudo")
    if [ "$OUT" ]; then
        # user has sudo permissions: use them to re-run the script
        sudo $script
        exit 0
    else
        echo "Please run script again with root privileges. Exiting."
        exit 1
    fi
fi


# Determine the correct device (i.e. not /dev/sda...)
echo "Removable disks found on this system:"
lsblk | grep -E 'sd[b-z]'
echo
echo "Which disk do you want to run in VirtualBox?"
disk_list=$(lsblk | grep -Eo 'sd[b-z]\s')
disk_list+=none
select d in $disk_list; do
    case $d in
        none) # user chooses to cancel & exit
            echo "No disk chosen. Exiting."
            exit 0
            ;;
        sd*) # user chooses a disk
            raw_disk=$d
            echo
            echo "You chose /dev/$raw_disk."
            echo "If this is not correct, press [Ctrl+C] to exit."
            read -p "Otherwise, press [Enter] to continue..."
            break
            ;;
        *) # invlalid choice
            echo "\"$REPLY\" is not a valid option. Try again:"
            ;;
    esac
done
echo

# Make disk availble to VirtualBox: Create VMDK virtual disk file & link to raw disk
echo "Virtual machine being created..."
echo
virt_disk_name="VirtualBox-USB-disk-$$.vmdk"
virt_mach_name="Temp-USB-VM-$$"
VBoxManage internalcommands createrawvmdk \
    -filename /tmp/"$virt_disk_name" -rawdisk /dev/$raw_disk

# Create new virtual machine
VBoxManage createvm --name "$virt_mach_name" --register

# Modify virtual machine properties
VBoxManage modifyvm "$virt_mach_name" \
    --memory 2048 \
    --pae on \
    --cpus 1 \
    --hwvirtex on \
    --nestedpaging on \
    --vram 16 \
    --nic1 nat

# Create storage controller
VBoxManage storagectl "$virt_mach_name" \
    --add sata --name "SATA" --portcount 1
    
# Attach virtual disk to storage controller
VBoxManage storageattach "$virt_mach_name" \
    --storagectl "SATA" --medium /tmp/"$virt_disk_name" --type hdd --port 0
echo
echo

# Start new virtual machine
echo "Starting virtual machine..."
echo
VBoxManage startvm "$virt_mach_name"
echo

# Cleanup: Unregister VM and delete files
read -p "Press [Enter] when done with the VM to remove its files."
echo
echo "Unregistering virtual machine and deleting files..."
VBoxManage unregistervm "$virt_mach_name" --delete
### Note: If this fails because the VM is "locked", there's probably a
### VBox process still running as root that is doing the locking. This
### process needs to be killed to properly unregister and delete the VM.
#if vbox process for this VM still running; then
    # kill vbox process for this VM
#fi
echo
echo "VM files removed. Exiting."

exit 0
