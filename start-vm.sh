#!/bin/bash

# Start, create, or remove VM for software testing.

show_help() {
    show_usage
    echo
    echo "  -h      Show this help text and exit."
    echo "  -l      List all VMs and exit."
    echo "  -r VM   Remove VM and associated files."
    echo "  -c ARG  Set # of CPUs. [3]"
    echo "  -d ARG  Set virtual hard disk size in MB. [32000]"
    echo "  -g ARG  Set VM groups. [\"/Wasta Test Suite\"]"
    echo "  -m ARG  Set memory allocation in MB. [4096]"
    echo "  -s ARG  Set VM system type. [\"Ubuntu (64-bit)\"]"
    echo "  -v ARG  Set vram value in MB. [128]"
    echo "  -x      Run in debug (set -x) mode"
    exit 0
}

show_usage() {
    echo "Usage:"
    echo "  $0 -h|-l"
    echo "  $0 -r NAME"
    echo "  $0 [-* ARG] NAME [/ISO/PATH]"
}

check_vm_drive() {
    local dir="$1"
    if [[ ! -d "$dir" ]]; then
        echo "ERROR: VM folder not available: $dir"
        exit 1
    fi
}

remove_vm() {
    local n="$1"
    VBoxManage showvminfo "$n" >/dev/null 2>&1
    if [[ $? -ne 0 ]]; then
        echo "ERROR: VM does not exist: $n"
        exit 1
    fi
    echo "Removing $n and associated files..."
    VBoxManage unregistervm --delete "$n"
    exit $?
}

basedir='/media/nate/VM Drive/VirtualBox VMs'

# These default variables can be changed with script options.
ostype="Ubuntu (64-bit)"
groups="/Wasta Test Suite"
cpus='3' # 4 can cause my 8-core laptop to lock up
mem='4096'
vram='128' # was 16
disk='32000'

# Parse options and args.
while getopts ":c:d:g:hlm:r:s:v:x" o; do
    case "$o" in
        c)
            cpus="$OPTARG"
            ;;
        d)
            disk="$OPTARG"
            ;;
        g)
            groups="$OPTARG"
            ;;
        h)
            show_help
            exit 0
            ;;
        l)
            # List VMs.
            check_vm_drive "$basedir"
            VBoxManage list --sorted vms
            exit 0
            ;;
        m)
            mem="$OPTARG"
            ;;
        r)
            # Remove VM.
            check_vm_drive "$basedir"
            remove_vm "$OPTARG"
            exit $?
            ;;
        s)
            ostype="$OPTARG"
            ;;
        v)
            vram="$OPTARG"
            ;;
        x)
            set -x
            ;;
        *)
            show_usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

# Check for required arg.
if [[ -z "$1" ]]; then
    show_usage
    exit 1
fi
name="$1"

# Ensure that VM drive is connected.
check_vm_drive "$basedir"

# Ensure VM exists.
VBoxManage showvminfo "$name" >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    # Initialize VM.
    echo "Creating VM..."
    VBoxManage createvm \
        --name "$name" \
        --groups "$groups" \
        --ostype "$ostype" \
        --basefolder "$basedir" \
        --register

    # Set system config.
    echo "Configuring VM..."
    VBoxManage modifyvm "$name" \
        --audio 'pulse' \
        --audioin 'off' \
        --audioout 'off' \
        --cableconnected1 'off' \
        --clipboard-mode 'bidirectional' \
        --cpus "$cpus" \
        --paravirtprovider 'kvm' \
        --graphicscontroller 'vmsvga' \
        --mouse 'usbtablet' \
        --ioapic 'on' \
        --memory "$mem" \
        --nic1 'nat' \
        --pae 'off' \
        --rtcuseutc 'on' \
        --usbxhci 'on' \
        --vram "$vram" \
        --x2apic 'on'
fi

# Set hd_file path. GUI-created VMs truncate the VDI filename if there's a '.'.
hd_file="${basedir}${groups}/${name}/${name}.vdi"
if [[ ! -e "$hd_file" ]]; then
    echo "Virtual hard disk not found: $hd_file"
    # Try file where "name" is truncated at '.'.
    if [[ -e "${basedir}${groups}/${name}/${name%%.*}.vdi" ]]; then
        hd_file="${basedir}${groups}/${name}/${name%%.*}.vdi"
    fi
fi

# Ensure HD is attached.
if [[ ! -e "$hd_file" ]]; then
    echo "Virtual hard disk not found: $hd_file"
    # Assume that the VM needs to be created and configured.
    echo "Creating and configuring new VM: ${name}."
    # Ensure ISO file is given.
    echo "Verifying ISO file..."
    iso_file="$2"
    if [[ ! -e "$iso_file" ]]; then
        echo "ERROR: Bad ISO file: $iso_file"
        exit 1
    fi

    # Add HD drive.
    echo "Adding HDD..."
    sata_storage='SATA'
    VBoxManage createhd \
        --filename "$hd_file" \
        --size "$disk" \
        --format 'VDI'
    VBoxManage storagectl "$name" \
        --name "$sata_storage" \
        --add 'sata' \
        --controller 'IntelAhci'
    VBoxManage storageattach "$name" \
        --storagectl "$sata_storage" \
        --port '0' \
        --device '0' \
        --type 'hdd' \
        --medium "$hd_file"

    # Insert ISO.
    echo "Adding DVD drive..."
    ide_storage='IDE'
    VBoxManage storagectl "$name" \
        --name "$ide_storage" \
        --add 'ide' \
        --controller 'PIIX4'
    VBoxManage storageattach "$name" \
        --storagectl "$ide_storage" \
        --port '1' \
        --device '0' \
        --type 'dvddrive' \
        --medium "$iso_file"

    # Set boot order.
    echo "Setting boot order..."
    VBoxManage modifyvm "$name" \
        --boot1 'dvd' \
        --boot2 'disk' \
        --boot3 'none' \
        --boot4 'none'
fi

# Start the VM.
VBoxManage startvm "$name" --type 'gui'
