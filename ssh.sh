#!/bin/bash

# Attach to screen instance on server for detachable SSH sessions.
#   If session "$2" exists, attach to it. Otherwise create new session.
# NOTE: To detatch from screen session, use Ctrl+A,D.
# Reference:
#   http://www.stearns.org/doc/screen-for-detachable-sessions.html

# Set local terminal window title.
echo -ne "\033]0;${1}: ${2:-default}\007"

if [[ "${2:0:1}" == '-' ]]; then
    # screen option passed; pass all remaining options.
    host="$1"
    shift
    ssh -t "$host" screen "$@"
    exit $?
fi

# Connect to screen instance via ssh.
ssh -t "$1" screen -d -R "${2:-default}"
