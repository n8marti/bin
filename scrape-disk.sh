#!/bin/bash


# Set default variables.
dev=/dev/sda
outdir="$PWD"
v='' # empty verbose option

show_help() {
    show_usage
    echo
    echo "  NAME    Filename used for output files NAME.img and NAME.map [disk]."
    echo "  -d DEV  Choose device to image with ddrescue. [$dev]"
    echo "  -h      Show this help text and exit."
    echo "  -o DIR  Choose ouput directory. [$PWD]"
    echo "  -v      Give verbose output."
    echo "  -x      Run in debug (set -x) mode"
    exit 0
}

show_usage() {
    echo "Usage:"
    echo "  $0 -h | [-v] [-x] [-d DEV] [-o DIR] [NAME]"
}

# Parse options and args.
while getopts ":d:ho:vx" o; do
    case "$o" in
        d)
            dev="$OPTARG"
            ;;
        h)
            show_help
            exit 0
            ;;
        o)
            outdir=$(realpath "$OPTARG")
            ;;
        v)
            v="-v"
            ;;
        x)
            set -x
            ;;
        *)
            show_usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

name="$1"

# Set output files.
if [[ -z $name ]]; then
    name="disk"
fi
img="${outdir}/${name}.img"
map="${outdir}/${name}.map"
touch "$img" "$map"
chown $v $SUDO_USER:$SUDO_USER "$img" "$map"

# Set block and copy chunk sizes.
blk=512
ch1=128
chunks="64 32 16 8 4 2 1"
if [[ "$dev" =~ /dev/sr[0-9]+ ]]; then
    # DVDs have blocksize of 2048 B.
    blk=2048
    ch1=32
    chunks="16 8 4 2 1"
fi

# First run.
echo "$(date +%T) Copy block size: $ch1"
ss1=$(date +%s)
ddrescue $v -b "$blk" -d "$dev" "$img" "$map"
status=$?
if [[ $status -ne 0 ]]; then
    exit $status
fi
es=$(date +%s)
duration=$(($es - $ss1))
echo "Finished in $(date -u -d@$duration +%Hh%Mm%Ss)"

# Susequent runs.
for c in $chunks; do
    echo -n "$(date +%T) Block copy size: $c"
    ss=$(date +%s)
    if [[ $c == '1' ]]; then
        echo
        ddrescue $v -b "$blk" -c$c -r3 -d "$dev" "$img" "$map"
        es=$(date +%s)
        echo "$(date +%T) Total time: $(date -u -d@$(($es - $ss1)) +%Hh%Mm%Ss)"
    else
        q='-q'
        if [[ $duration -gt 1800 || -n $v ]]; then
            echo
            q=''
        fi
        ddrescue $v $q -b "$blk" -c$c -r3 -d "$dev" "$img" "$map"
        es=$(date +%s)
        echo "; finished in $(date -u -d@$(($es - $ss)) +%Hh%Mm%Ss)"
    fi
done
chown $v $SUDO_USER:$SUDO_USER "$img" "$map"
