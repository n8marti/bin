#!/usr/bin/env python3

"""Accept a text file as the argument and output only the lines containing accented characters."""

import sys

from pathlib import Path

infile = Path(sys.argv[1]).resolve()
outdir = infile.parent
outfile = infile.parent / f"{infile.name}_filtered.txt"

accented_lines = []
with open(infile) as f:
    lines = f.readlines()

for line in lines:
    for c in line:
        if ord(c) >= 192:
            # print(c, line.strip())
            accented_lines.append(line.strip())
            break

accented_lines = list(set(accented_lines))
accented_lines.sort()

text = '\n'.join(accented_lines)
outfile.write_text(text)

print(f"{len(accented_lines)} unique accented words")
