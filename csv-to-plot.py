#!/usr/bin/env python3

"""
This is a basic script to import text from a CSV file where there is one
column with each cell containing multi-word items that were chosen from a list.
The script outputs a plot showing each item on the X-axis and a vote count on
the Y-axis.
"""

import csv
import sys
import textwrap
import numpy as np
import matplotlib.pyplot as plt

from pathlib import Path


def force_wrap(input_text, wraplength=10):
    return textwrap.fill(input_text, wraplength, break_long_words=False, replace_whitespace=False)


# Set global variables.
infile_path = Path(sys.argv[1]).resolve()
if infile_path.name == 'team-health-top-issues.csv':
    title = "Top Issues"
    choices = [
        "loneliness",
        "loss of group vision (the \"why\")",
        "mental health/negative thinking",
        "motivation to do my daily tasks (the \"what\")",
        "physical health",
        "spiritual health",
        "travel restrictions",
        "other",
        "no response",
    ]
    labelsize = 12
    wraplength = 25
    leftmargin = 0.2
    textpos = (0.5, 0.87)
    textha = 'left'
    textva = 'top'
    textsize = labelsize-4
elif infile_path.name == 'team-health-helpful-activities.csv':
    title = "Helpful Activities"
    choices = [
        "Attending online SIL Global Staff meetings with the executive leaders",
        "Calling/Writing a teammate when I need encouragement or help (work-related or not)",
        "Connecting with a counselor or pastor",
        "Connecting with a mentor or coach",
        "Having clear work goals",
        "Interacting with ACATBA's prayer meeting notes",
        "Making works trips to CAR",
        "Participating in weekly team Skype chats",
        "Participating in the daily online team prayer at \"The Well\"",
        "Participating in the monthly online team prayer",
        "Sharing with and feeling supported by an outside prayer group (church, Bible study, etc.)",
        "Reading Africa Impressions",
        "Receiving regular team news in the monthly Director's Bulletin",
        "Regular contact with my supervisor",
        "Regular contact with my domain team",
        "Regular prayer times alone or with a prayer partner",
        "other",
        "no response",
    ]
    labelsize = 10
    wraplength = 100
    leftmargin = 0.45
    textpos = (0.55, 0.115)
    textha = 'left'
    textva = 'bottom'
    textsize = labelsize
else:
    print(f"Error: Bad input file.")
    exit(1)

# Collect raw data from CSV file.
data_orig = []
with open(infile_path) as csvf:
    reader = csv.reader(csvf)
    for row in reader:
        if row:
            data_orig.append(row[0]) # only 1 column, so always choose 1st item
        else: # empty row
            data_orig.append(None)

# Convert raw data to useful data.
r_ct = 0
a_ct = 0
data_combined = {c:[0, []] for c in choices}
for cell in data_orig:
    r_ct += 1
    if r_ct == 1: # skip heading
        continue
    a_ct += 1
    # print(f"# {cell = }")
    if cell:
        parts = cell.split(',')
        parts = [p.strip() for p in parts]
        other_parts = []
        selections = []
        for p in parts:
            # print(f"{p = }")

            # Determine if text part corresponds to a selection.
            found = False
            for c in choices:
                if c not in selections and p in c:
                    found = True
                    # print(f"-- \"{p}\" found in: {c}")
                    selections.append(c)
                    data_combined[c][0] += 1
                    break

            # Determine if text part has already been counted.
            p_in_selections = False
            for s in selections:
                if p in s:
                    p_in_selections = True
                    break

            # Add to "other" if necessary.
            if not found and not p_in_selections:
                other_parts.append(p)

        # Save and count "other" parts.
        if other_parts:
            other_text = ', '.join(other_parts)
            # print(f"{other_text = }")
            data_combined['other'][0] += 1
            data_combined['other'][1].append(other_text)
    else:
        data_combined['no response'][0] += 1

# print(data_combined)
# print('; '.join(data_combined.get('other')[1]))
# print("Other responses:")
# for i, o in enumerate(data_combined.get('other')[1]):
#     print(f"{i+1}: {o}")

# Prepare Other text.
plot_text_list = ["Other responses:"]
plot_text_list.extend(data_combined.get('other')[1])
plot_text = '\n'.join(plot_text_list)

# Name the plot's data series.
answers = data_combined.keys()
counts = [c for c, t in data_combined.values()]

# Build the plot.
plt.style.use('Solarize_Light2')
# print(plt.style.available)
# exit()
y_pos = np.arange(len(answers))
fig, ax = plt.subplots(figsize=(16,9))
plt.subplots_adjust(left=leftmargin)
ax.barh(y_pos, counts, height=0.90)

# Make adjustments.
ax.set_yticks(y_pos)
ax.set_yticklabels([force_wrap(a, wraplength=wraplength) for a in answers])
ax.invert_yaxis()
ax.tick_params(axis='y', labelsize=labelsize)
ax.set_title(title)
ax.set(xlim=(0, max(counts)+1), xticks=np.arange(1, max(counts)+1))

# Add text box.
# plt.figtext(
#     textpos[0],
#     textpos[1],
#     force_wrap(plot_text, wraplength=100),
#     ha=textha,
#     va=textva,
#     fontsize=textsize
# )

plt.savefig(infile_path.with_suffix('.png'))
plt.show()
