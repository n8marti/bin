#!/bin/bash

# Alternate lines from two text files. Potentially useful for Paratext EAB/SAB files.

sed "R $2" "$1"

exit $?

paste -d '\n' "$1" "$2"
