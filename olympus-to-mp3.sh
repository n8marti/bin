#!/bin/bash

# Convert audio files from Elizabeth's Olympus voice recorder to mp3 format.
if [[ $1 == '-h' ]] || [[ $1 == '--help' ]]; then
    echo "Convert DSS files from Olympus voice recorder to MP3. Save to Downloads."
    exit 0
fi

# Determine the input file.
if [[ -r $1 ]]; then
    ifile="$1"
else
    ifile=$(zenity --file-selection                             \
            --title="Choose a DSS file to convert."             \
            --file-filter='DSS audio files (.dss) | *.dss *.DSS'\
            --filename="/media/$USER/"                          \
            2>/dev/null                                         \
            )
    if [[ $? -ne 0 ]] || [[ ! -r $ifile ]]; then
        exit 1
    else
        filename="${ifile##*/}"
        ofile="$HOME/${filename%.*}.mp3"
    fi
fi

# Convert the file.
ffmpeg -codec dss_sp -i "$ifile" -codec mp3 "$ofile"

# Show the converted file.
zenity --file-selection          \
        --title="Converted file."\
        --filename="$ofile"      \
        2>/dev/null
