#!/bin/bash


filter_whois() {
    # Echo relevant whois details.
    data="$1"
    range=$(echo "$data" | grep NetRange | awk '{print $2, $3, $4}')
    inetnum=$(echo "$data" | grep inetnum | awk '{print $2, $3, $4}')
    cidr=$(echo "$data" | grep CIDR | awk '{print $2}')
    netname=$(echo "$data" | grep -i netname | awk '{print $2}')
    org=$(echo "$data" | grep Organization | tr -s ' ' | cut -d' ' -f2-)

    # Show owner.
    if [[ -n "$org" ]]; then
        echo "Org.: $org"
    elif [[ -n "$netname" ]]; then
        echo "Name: $netname"
    fi

    # Show addresses.
    if [[ -n "$cidr" ]]; then
        echo "CIDR: $cidr"
    elif [[ -n "$range" ]]; then
        echo "IPs : $range"
    elif [[ -n "$inetnum" ]]; then
        echo "IPs : $inetnum"
    fi
}

get_whois() {
    whois "$1"
}

get_fqdn() {
    dig @1.1.1.1 -x "$1"
}

filter_fqdn() {
    # Echo relevant FQDN details.
    data="$1"
    ans=$(echo "$data" | grep -A1 '^;; ANSWER SECTION:$' | tail -n1 | awk '{print $5}')
    echo "FQDN: ${ans%*.}"
}


VERBOSE=
while getopts ":hv" opt; do
    case $opt in
        h) # help text
            help_text="Help!"
            echo "$help_text"
            exit 0
            ;;
        v) # verbose
            VERBOSE=yes
            ;;
        \?) # message for invalid option
            echo "$usage"
            exit 3
            ;;
    esac
done
shift $(($OPTIND - 1))
IPs=($@)

for ip in "${IPs[@]}"; do
    whois_data=$(get_whois "$ip")
    fqdn_data=$(get_fqdn "$ip")

    echo "IPv4: $ip"
    if [[ -n "$VERBOSE" ]]; then
        echo
        echo "### WHOIS:"
        echo "$whois_data"
        echo
        echo "### DIG:"
        echo "$fqdn_data"
    else
        filter_whois "$whois_data"
        filter_fqdn "$fqdn_data"
    fi
    echo
done
